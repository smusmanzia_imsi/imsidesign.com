<?php
/**
 * @package     perfectdashboard
 * @version     1.12
 *
 * @copyright   Copyright (C) 2017 Perfect Dashboard. All rights reserved. https://perfectdashboard.com
 * @license     GNU General Public Licence http://www.gnu.org/licenses/gpl-3.0.html
 */

defined('_JEXEC') or die;

$html_tag = 'div';
include dirname(__FILE__) . '/connect_form.php'
?>
<button type="button" onclick="document.getElementById('perfectdashboard_connect_form').submit()"
        class="btn btn-large btn-primary">
	<?php echo JText::_('PLG_PERFECTDASHBOARD_CONNECT_WEBSITE_MESSAGE'); ?>
</button>
<script type="text/javascript">
	<?php if (version_compare(JVERSION, '3.0.0', '>=')) : ?>
    jQuery(document).ready(function ($) {
        var form = $("#perfectdashboard_connect_form");
        form.insertAfter(document.adminForm);
        form[0].outerHTML = form[0].outerHTML.replace("div","form");
    });
	<?php else : ?>
    window.addEvent("domready", function () {
        var form = document.id("perfectdashboard_connect_form");
        form.inject(document.adminForm, "after");
        form.outerHTML = form.outerHTML.replace("div","form");
    });
	<?php endif; ?>
</script>