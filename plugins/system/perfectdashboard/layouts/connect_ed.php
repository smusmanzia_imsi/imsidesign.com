<?php
/**
 * @package     perfectdashboard
 * @version     1.12
 *
 * @copyright   Copyright (C) 2017 Perfect Dashboard. All rights reserved. https://perfectdashboard.com
 * @license     GNU General Public Licence http://www.gnu.org/licenses/gpl-3.0.html
 */

defined('_JEXEC') or die;
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title>Perfect Dashboard</title>
</head>
<body>
<form action="<?php echo PerfectDashboard_Config::getPdUrl(); ?>site/connect?utm_campaign=ed&utm_medium=affiliation&utm_source=<?php echo $referral_key; ?>&utm_content=updater"
      method="post" enctype="multipart/form-data" name="pd_installer">

	<?php include dirname(__FILE__) . '/connect_form_fields.php' ?>
    <input type="hidden" name="referral_key" value="<?php echo $referral_key; ?>">

    Redirecting to Perfect Dashboard...
    <button type="submit" id="pd_submit">Click to continue</button>
</form>
<script type="text/javascript">
    var pd_submit = document.getElementById("pd_submit");
    pd_submit.style.display = "none";
    document.pd_installer.submit();
    setTimeout(function () {
        pd_submit.style.display = ""
    }, 1000);
</script>
</body>
</html>