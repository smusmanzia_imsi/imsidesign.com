<?php
/**
 * @package     perfectdashboard
 * @version     1.12
 *
 * @copyright   Copyright (C) 2017 Perfect Dashboard. All rights reserved. https://perfectdashboard.com
 * @license     GNU General Public Licence http://www.gnu.org/licenses/gpl-3.0.html
 */

defined('_JEXEC') or die;
?>

<div id="perfectdashboard_connect_message">
    <p style="margin: 25px 0 0 80px; font-size: 16px; display: inline-block;">
        <img src="https://perfectdashboard.com/assets/images/shield.svg" alt="Perfect Dashboard"
             style="float: left; width: 60px; margin: -10px 0 0 -70px;">
        <strong><?php echo JText::_('PLG_PERFECTDASHBOARD_INSTALLATION_SUCCESS_HEADER'); ?></strong><br>
		<?php echo JText::sprintf('PLG_PERFECTDASHBOARD_INSTALLATION_SUCCESS_MESSAGE', '<strong>Perfect Dashboard</strong>'); ?>
    </p>
    <button type="button" onclick="document.getElementById('perfectdashboard_connect_form').submit()"
            class="btn btn-large btn-primary" style="margin: 25px 0 25px 20px; vertical-align: top;">
		<?php echo JText::_('PLG_PERFECTDASHBOARD_INSTALLATION_SUCCESS_BUTTON'); ?>
    </button>

	<?php include dirname(__FILE__) . '/connect_form.php'; ?>
</div>
<style type="text/css">
    <?php if (version_compare(JVERSION, '3.0', '>=')) : ?>
    .alert.alert-info.j-jed-message,
    #system-message-container > *:not(.alert-success),
    #system-message-container .alert-heading,
    #system-message-container .alert-success .alert-message {
        display: none !important
    }
    <?php else : ?>
    #system-message > *:not(.message.message) {
        display: none !important
    }
    <?php endif; ?>
</style>
<script type="text/javascript">
    (function () {
        document.getElementById("perfectdashboard_connect_message")
            .parentElement.setAttribute("style", "display:block !important")
    })()
</script>