<?php
defined('PERFECTDASHBOARD_LIB') or die;

if (!defined('PERFECTDASHBOARD_STAGE'))
{
	define('PERFECTDASHBOARD_STAGE', 'app');
}
if (!empty($_REQUEST['pd_callback']))
{
	// callback syntax: "subdomain:port:protocol". Example: "app:443:https" or just "app"
	define('PERFECTDASHBOARD_CALLBACK', preg_replace('/[^a-z0-9:]/', '', $_REQUEST['pd_callback']));
}

if (!defined('PERFECTDASHBOARD_CMS'))
{
	define('PERFECTDASHBOARD_CMS', null);
}
if (!defined('PERFECTDASHBOARD_VERSION'))
{
	define('PERFECTDASHBOARD_VERSION', '1.0');
}
if (!defined('PERFECTDASHBOARD_SITE_PATH'))
{
	define('PERFECTDASHBOARD_SITE_PATH', dirname(dirname(__FILE__)) . '/');
}

if (PERFECTDASHBOARD_STAGE != 'app')
{
	define('PERFECTDASHBOARD_DEBUG', true);
}

if (!defined('PERFECTDASHBOARD_LIB_PATH'))
{
	define('PERFECTDASHBOARD_LIB_PATH', dirname(__FILE__) . '/');

	require_once PERFECTDASHBOARD_LIB_PATH . 'Loader.php';
	require_once PERFECTDASHBOARD_LIB_PATH . 'Config.php';

	require_once PERFECTDASHBOARD_LIB_PATH . 'Api.php';
	require_once PERFECTDASHBOARD_LIB_PATH . 'Authentication.php';
	require_once PERFECTDASHBOARD_LIB_PATH . 'Backuptool.php';
	require_once PERFECTDASHBOARD_LIB_PATH . 'Db.php';
	require_once PERFECTDASHBOARD_LIB_PATH . 'Filemanager.php';
	require_once PERFECTDASHBOARD_LIB_PATH . 'Log.php';
	require_once PERFECTDASHBOARD_LIB_PATH . 'Request.php';
	require_once PERFECTDASHBOARD_LIB_PATH . 'Response.php';
	require_once PERFECTDASHBOARD_LIB_PATH . 'Task.php';

	require_once PERFECTDASHBOARD_LIB_PATH . 'Exception/Response.php';

	if (file_exists(PERFECTDASHBOARD_LIB_PATH . 'vendor/autoload.php'))
	{
		require_once PERFECTDASHBOARD_LIB_PATH . 'vendor/autoload.php';
	}
}