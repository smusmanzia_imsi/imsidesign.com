<?php
defined('PERFECTDASHBOARD_LIB') or die;

class PerfectDashboard_Config
{
	protected static $instance = null;

	/**
	 * @return static
	 */
	protected static function getInstance()
	{
		if (!is_null(static::$instance))
		{
			return static::$instance;
		}

		$class_name = PerfectDashboard_Loader::loadClass('Config');

		static::$instance = new $class_name();

		return static::$instance;
	}

	/**
	 * @param string     $key
	 * @param null|mixed $default
	 *
	 * @return bool
	 */
	public static function get($key, $default = null)
	{
		if ($key == 'debug' && defined('PERFECTDASHBOARD_DEBUG') && PERFECTDASHBOARD_DEBUG)
		{
			return true;
		}

		return static::getInstance()->getOption($key, $default);
	}

	/**
	 * @param string $key
	 * @param mixed  $value
	 *
	 * @return bool
	 */
	public static function set($key, $value)
	{
		return static::getInstance()->setOption($key, $value);
	}

	/**
	 * @return bool
	 */
	public static function removeAll()
	{
		return static::getInstance()->removeAllOptions();
	}

	/**
	 * @return string
	 */
	public static function getSiteUrl()
	{
		return rtrim(static::getInstance()->getOptionSiteUrl(), '/');
	}

	/**
	 * @return string
	 */
	public static function getSiteBackendUrl()
	{
		return rtrim(static::getInstance()->getOptionSiteBackendUrl(), '/');
	}

	/**
	 * @return string
	 */
	public static function getPdUrl()
	{
		// Callback syntax: "subdomain:port:protocol". Example: "app:443:https" or just "app"
		if (!defined('PERFECTDASHBOARD_CALLBACK'))
		{
			return 'https://' . PERFECTDASHBOARD_STAGE . '.perfectdashboard.com/';
		}

		@list($subdomain, $port, $protocol) = explode(':', PERFECTDASHBOARD_CALLBACK);

		return ($protocol == 'http' ? 'http' : 'https') . '://'
			. $subdomain . '.perfectdashboard.com'
			. ($port > 0 ? ':' . (int) $port : '')
			. '/';
	}

	/**
	 * @param string $key
	 *
	 * @return bool
	 */
	public static function remove($key)
	{
		return static::getInstance()->removeOption($key);
	}

	protected function getOptionSiteUrl()
	{
		return '';
	}

	protected function getOptionSiteBackendUrl()
	{
		return '';
	}

	protected function getOption($key, $default = null)
	{
		return $default;
	}

	protected function setOption($key, $value)
	{
		return true;
	}

	protected function removeOption($key)
	{
		return true;
	}

	protected function removeAllOptions()
	{
		return true;
	}
}