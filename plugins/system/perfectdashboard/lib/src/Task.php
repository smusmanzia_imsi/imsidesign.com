<?php
defined('PERFECTDASHBOARD_LIB') or die;

require_once PERFECTDASHBOARD_LIB_PATH . 'Task/Base.php';

class PerfectDashboard_Task
{
	protected static $instances = array();

	/**
	 * @param string     $task
	 * @param array|null $payload
	 *
	 * @return static
	 * @throws Exception
	 */
	public static function getInstance($task, $payload = null)
	{
		if (isset(static::$instances[$task]))
		{
			return static::$instances[$task];
		}

		$class_name = PerfectDashboard_Loader::loadClass('Task_' . $task);
		if (!$class_name)
		{
			throw new Exception('Unknow task');
		}

		static::$instances[$task] = new $class_name($payload);

		return static::$instances[$task];
	}
}