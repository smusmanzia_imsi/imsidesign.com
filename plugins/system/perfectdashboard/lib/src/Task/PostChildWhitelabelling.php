<?php
defined('PERFECTDASHBOARD_LIB') or die;

class PerfectDashboard_Task_PostChildWhitelabelling extends PerfectDashboard_Task_Base
{
	/**
	 * @return array
	 */
	public function doTask()
	{
		$name          = $this->input('name');
		$author        = $this->input('author');
		$child_page    = $this->input('child_page');
		$login_page    = $this->input('login_page');
		$protect_child = (int) $this->input('protect_child');
		$hide_child    = (int) $this->input('hide_child');

		if (empty($name))
		{
			PerfectDashboard_Config::remove('whitelabel_name');
		}
		else
		{
			PerfectDashboard_Config::set('whitelabel_name', $name);
		}

		if (empty($author))
		{
			PerfectDashboard_Config::remove('whitelabel_author');
		}
		else
		{
			PerfectDashboard_Config::set('whitelabel_author', $author);
		}

		if (empty($child_page))
		{
			PerfectDashboard_Config::remove('whitelabel_child_page');
		}
		else
		{
			PerfectDashboard_Config::set('whitelabel_child_page', $child_page);
		}

		if (empty($login_page))
		{
			PerfectDashboard_Config::remove('whitelabel_login_page');
		}
		else
		{
			PerfectDashboard_Config::set('whitelabel_login_page', $login_page);
		}

		PerfectDashboard_Config::set('protect_child', $protect_child);
		PerfectDashboard_Config::set('hide_child', $hide_child);

		return array(
			'success' => true,
		);
	}
}