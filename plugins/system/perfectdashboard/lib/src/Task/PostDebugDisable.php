<?php
defined('PERFECTDASHBOARD_LIB') or die;

class PerfectDashboard_Task_PostDebugDisable extends PerfectDashboard_Task_Base
{
	/**
	 * @return array
	 */
	public function doTask()
	{
		$debug = (bool) PerfectDashboard_Config::get('debug');
		if ($debug && PerfectDashboard_Config::set('debug', false))
		{
			$debug = false;
		}

		return array(
			'success' => ($debug === false)
		);
	}
}