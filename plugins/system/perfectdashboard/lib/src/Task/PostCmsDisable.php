<?php
defined('PERFECTDASHBOARD_LIB') or die;

class PerfectDashboard_Task_PostCmsDisable extends PerfectDashboard_Task_Base
{
	/**
	 * @return array
	 */
	public function doTask()
	{
		$offline = (bool) PerfectDashboard_Config::get('offline');
		if (!$offline && PerfectDashboard_Config::set('offline', true))
		{
			$offline = true;
		}

		return array(
			'success'    => ($offline === true),
			'is_offline' => $offline,
		);
	}
}