<?php
defined('PERFECTDASHBOARD_LIB') or die;

class PerfectDashboard_Task_PostBackuptoolSetup extends PerfectDashboard_Task_Base
{
	/**
	 * @return array
	 */
	public function doTask()
	{
		$options = array(
			'htaccess_disable' => (bool) $this->input('htaccess_disable', false),
			'backup_part_size' => (int) $this->input('backup_part_size', 0),
		);

		return PerfectDashboard_Backuptool::getInstance()
			->setup(null, null, null, null, $options);
	}
}