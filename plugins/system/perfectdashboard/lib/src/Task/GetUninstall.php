<?php
defined('PERFECTDASHBOARD_LIB') or die;

class PerfectDashboard_Task_GetUninstall extends PerfectDashboard_Task_Base
{
	/**
	 * @return array
	 */
	public function doTask()
	{
		$result = PerfectDashboard_Installer::getInstance()
			->uninstall();

		return array(
			'success' => $result
		);
	}
}