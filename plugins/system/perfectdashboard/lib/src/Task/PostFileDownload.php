<?php
defined('PERFECTDASHBOARD_LIB') or die;

class PerfectDashboard_Task_PostFileDownload extends PerfectDashboard_Task_Base
{
	/**
	 * @throws PerfectDashboard_Exception_Response
	 * @throws Exception
	 *
	 * @return array
	 */
	public function doTask()
	{
		$url = $this->input('file_url');
		if (!$url)
		{
			throw new PerfectDashboard_Exception_Response('Nothing to download', 400);
		}

		$filemanager = PerfectDashboard_Filemanager::getInstance();
		$path        = $filemanager->download($url);

		return array(
			'success' => $path ? true : false,
			'return'  => array(
				'file_path' => $filemanager->trimPath($path),
			),
		);
	}
}