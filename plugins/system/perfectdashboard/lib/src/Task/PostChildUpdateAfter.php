<?php
defined('PERFECTDASHBOARD_LIB') or die;

class PerfectDashboard_Task_PostChildUpdateAfter extends PerfectDashboard_Task_Base
{
	/**
	 * @return array
	 */
	public function doTask()
	{
		$installer = PerfectDashboard_Installer::getInstance()
			->setOption('site_id', (int) $this->input('site_id'));

		if (!$installer->update())
		{
			return array(
				'success' => false,
				'message' => 'Failed to finish update',
			);
		}

		return array(
			'success' => true,
		);
	}
}