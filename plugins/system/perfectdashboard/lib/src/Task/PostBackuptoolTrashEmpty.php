<?php
defined('PERFECTDASHBOARD_LIB') or die;

class PerfectDashboard_Task_PostBackuptoolTrashEmpty extends PerfectDashboard_Task_Base
{
	/**
	 * @return array
	 */
	public function doTask()
	{
		$result      = true;
		$path        = PERFECTDASHBOARD_SITE_PATH . 'perfectdashboard_oldfiles/';
		$filemanager = PerfectDashboard_Filemanager::getInstance();

		if ($filemanager->is_dir($path))
		{
			$trash_bin = basename($this->input('trash_bin'));
			if ($trash_bin)
			{
				// Remove the given trash bin
				if ($filemanager->is_dir($path . $trash_bin))
				{
					$result = $filemanager->rmdir($path . $trash_bin, true);
				}
			}
			else
			{
				// Remove all trash bins
				$result = $filemanager->rmdir($path, true);
			}
		}

		return array(
			'success' => $result,
		);
	}
}