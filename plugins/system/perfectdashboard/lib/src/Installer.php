<?php
defined('PERFECTDASHBOARD_LIB') or die;

class PerfectDashboard_Installer
{
	protected static $instance = null;
	protected $options = array();

	/**
	 * @return static
	 */
	public static function getInstance()
	{
		if (!is_null(static::$instance))
		{
			return static::$instance;
		}

		$class_name = PerfectDashboard_Loader::loadClass('Installer');

		static::$instance = new $class_name();

		return static::$instance;
	}

	public function __construct()
	{

	}

	/**
	 * @param string $key
	 * @param mixed  $value
	 *
	 * @return $this
	 */
	public function setOption($key, $value)
	{
		$this->options[$key] = $value;

		return $this;
	}

	/**
	 * @param string $key
	 * @param mixed  $default
	 *
	 * @return mixed
	 */
	public function getOption($key, $default = null)
	{
		if (isset($this->options[$key]))
		{
			return $this->options[$key];
		}

		return $default;
	}

	/**
	 * @return bool
	 */
	public function install()
	{
		PerfectDashboard_Log::debug(sprintf('Installing Child %s', PERFECTDASHBOARD_VERSION));

		if (!PerfectDashboard_Config::get('version'))
		{
			PerfectDashboard_Config::set('version', PERFECTDASHBOARD_VERSION);
		}

		$this->createTokens();

		PerfectDashboard_Log::debug(sprintf('Child %s has been installed.', PERFECTDASHBOARD_VERSION));

		return true;
	}

	public function selfUpdate()
	{
		if (isset($_REQUEST['pd_endpoint']) && $_REQUEST['pd_endpoint'] == 'child/update/after')
		{
			// Do not run self-update as we are running it through API
			return;
		}

		$version = PerfectDashboard_Config::get('version', '1.0');
		if (version_compare($version, PERFECTDASHBOARD_VERSION, '<'))
		{
			PerfectDashboard_Log::debug("Self update from version $version to " . PERFECTDASHBOARD_VERSION);
			$this->update();
		}
	}

	/**
	 * @return bool
	 */
	public function update()
	{
		$current_version = PerfectDashboard_Config::get('version', '1.0');
		$new_version     = $this->getOption('version', PERFECTDASHBOARD_VERSION);
		PerfectDashboard_Log::debug(sprintf('Updating Child from version %s to %s', $current_version, $new_version));

		if (version_compare($current_version, '1.12', '<'))
		{
			if (!$this->migrateVersion1_12())
			{
				return false;
			}
		}

		if (version_compare($current_version, $new_version, '<'))
		{
			PerfectDashboard_Config::set('version', $new_version);
		}

		PerfectDashboard_Log::debug(sprintf('Child has been updated from version %s to %s', $current_version, $new_version));

		return true;
	}

	/**
	 * @return bool
	 */
	protected function migrateVersion1_12()
	{
		return true;
	}

	/**
	 * @return bool
	 */
	protected function createTokens()
	{
		if (!PerfectDashboard_Config::get('read_token'))
		{
			PerfectDashboard_Config::set('read_token', $this->generateToken());

			if (!PerfectDashboard_Config::get('write_token'))
			{
				PerfectDashboard_Config::set('write_token', $this->generateToken());
			}
		}

		return true;
	}

	/**
	 * @return bool
	 */
	protected function recreateTokens()
	{
		$read_token  = $this->generateToken();
		$write_token = $this->generateToken();

		$json = array(
			'read_token'  => $read_token,
			'write_token' => $write_token,
		);
		if (!empty($this->options['site_id']))
		{
			$json['site_id'] = (int) $this->options['site_id'];
		}
		else
		{
			$json['site_url'] = PerfectDashboard_Config::getSiteUrl();
		}
		$json = json_encode($json);

		$signature = PerfectDashboard_Authentication::getInstance()
			->getSignature(array(
				'json' => $json,
			));

		$response = PerfectDashboard_Request::post(
			PerfectDashboard_Config::getPdUrl() . 'api/1.0/child/site/tokens?pd_signature=' . $signature,
			$json,
			array(
				'Content-Type' => 'application/json',
			)
		);

		if ($response->code == 200 && !empty($response->body->success))
		{
			PerfectDashboard_Config::set('read_token', $read_token);
			PerfectDashboard_Config::set('write_token', $write_token);

			return true;
		}

		// Limit the number of attempts to recreate tokens
		$token_update_attempts = (int) PerfectDashboard_Config::get('token_update_attempts', 0);
		$token_update_attempts++;

		if ($token_update_attempts > 5)
		{
			return true;
		}

		PerfectDashboard_Config::set('token_update_attempts', $token_update_attempts);

		return false;
	}

	/**
	 * @return string
	 */
	protected function generateToken()
	{
		$key   = '';
		$chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$max   = mb_strlen($chars, '8bit') - 1;
		for ($i = 0; $i < 32; ++$i)
		{
			$key .= $chars[random_int(0, $max)];
		}

		return $key;
	}

	/**
	 * @return bool
	 */
	public function uninstall()
	{
		PerfectDashboard_Log::debug(sprintf('Uninstalling Child %s', PERFECTDASHBOARD_VERSION));

		PerfectDashboard_Backuptool::getInstance()
			->uninstall();

		PerfectDashboard_Config::removeAll();

		PerfectDashboard_Log::debug(sprintf('Child %s has been uninstalled.', PERFECTDASHBOARD_VERSION));

		return true;
	}
}