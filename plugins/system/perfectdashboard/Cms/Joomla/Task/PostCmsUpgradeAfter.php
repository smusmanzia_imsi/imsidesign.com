<?php
defined('_JEXEC') or die;

class PerfectDashboard_Cms_Joomla_Task_PostCmsUpgradeAfter extends PerfectDashboard_Task_PostCmsUpgradeAfter
{
	/**
	 * @return array
	 */
	public function doTask()
	{
		return PerfectDashboard_Task::getInstance('PostCmsUpdateAfter', $this->payload)
			->doTask();
	}
}