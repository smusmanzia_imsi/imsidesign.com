<?php
defined('_JEXEC') or die;

class PerfectDashboard_Task_Cms_Joomla_PostFilesList extends PerfectDashboard_Task_PostFilesList
{
	/**
	 * @return array
	 */
	protected function getDefaultExclusions()
	{
		$filemanager = PerfectDashboard_Filemanager::getInstance();
		$logger      = PerfectDashboard_Log::getInstance();
		$admin_path  = basename(JPATH_ADMINISTRATOR) . '/';

		$exclusions = array_merge(parent::getDefaultExclusions(), array(
			$admin_path . 'cache',
			$admin_path . 'logs', // default logs path J! >= 3.6
			'media/template', // Yoo Theme cache
			str_replace(PERFECTDASHBOARD_SITE_PATH, '',
				$filemanager->untrailingslashit($logger->getLogsPath())
			), // config logs path
			str_replace(PERFECTDASHBOARD_SITE_PATH, '',
				$filemanager->untrailingslashit($filemanager->getTempPath())
			), // config temp path
		));

		return array_unique($exclusions);
	}
}