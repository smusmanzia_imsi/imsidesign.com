<?php
/**
 * @package     perfectdashboard
 * @version     1.12
 *
 * @copyright   Copyright (C) 2017 Perfect Dashboard. All rights reserved. https://perfectdashboard.com
 * @license     GNU General Public Licence http://www.gnu.org/licenses/gpl-3.0.html
 */

defined('_JEXEC') or die;

class JFormFieldConnect extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var        string
	 * @since    1.6
	 */
	protected $type = 'Connect';

	/**
	 * Do not show label
	 *
	 * @var    boolean
	 * @since  11.1
	 */
	protected $hidden = false;

	/**
	 * Method to get the field input markup.
	 *
	 * @return    string    The field input markup.
	 * @since    1.6
	 */
	protected function getInput()
	{
		if (!class_exists('PerfectDashboard_Config'))
		{
			JFactory::getApplication()
				->enqueueMessage(JText::_('PLG_PERFECTDASHBOARD_ENABLE_PLUGIN_ERROR'), 'error');
			return '';
		}

		if ($whitelabel_child_page = PerfectDashboard_Config::get('whitelabel_child_page'))
		{
			return $whitelabel_child_page;
		}

		// add documentation toolbar button
		if (version_compare(JVERSION, '3.0.0', '<'))
		{
			$button = '<a href="#" onclick="document.getElementById(\'perfectdashboard_connect_form\').submit()"'
				. ' style="font-weight:bold;border-color:#025A8D;background-color:#DBE4E9;">'
				. JText::_('PLG_PERFECTDASHBOARD_CONNECT_WEBSITE')
				. '</a>';
		}
		else
		{
			$button = '<button onclick="document.getElementById(\'perfectdashboard_connect_form\').submit()"'
				. ' class="btn btn-small btn-primary">'
				. JText::_('PLG_PERFECTDASHBOARD_CONNECT_WEBSITE')
				. '</button>';
		}
		$bar = JToolBar::getInstance();
		$bar->appendButton('Custom', $button, '-docs');

		// Variable used in file included below.
		$html_tag = 'div';

		ob_start();
		include PERFECTDASHBOARD_J_PLUGIN_PATH . 'layouts/connect_configuration.php';

		return ob_get_clean();
	}
}