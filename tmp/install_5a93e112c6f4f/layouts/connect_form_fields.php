<?php
/**
 * @package     perfectdashboard
 * @version     1.12
 *
 * @copyright   Copyright (C) 2017 Perfect Dashboard. All rights reserved. https://perfectdashboard.com
 * @license     GNU General Public Licence http://www.gnu.org/licenses/gpl-3.0.html
 */

defined('_JEXEC') or die;
?>
<input type="hidden" name="read_token" value="<?php echo PerfectDashboard_Config::get('read_token'); ?>">
<input type="hidden" name="write_token" value="<?php echo PerfectDashboard_Config::get('write_token'); ?>">
<input type="hidden" name="user_email" value="<?php echo JFactory::getUser()->get('email'); ?>">
<input type="hidden" name="site_frontend_url" value="<?php echo PerfectDashboard_Config::getSiteUrl(); ?>">
<input type="hidden" name="site_backend_url" value="<?php echo PerfectDashboard_Config::getSiteBackendUrl(); ?>">
<input type="hidden" name="cms_type" value="joomla">
<input type="hidden" name="cms_version" value="<?php echo JVERSION; ?>">
<input type="hidden" name="version" value="<?php echo !empty($version) ? $version : PERFECTDASHBOARD_VERSION; ?>">