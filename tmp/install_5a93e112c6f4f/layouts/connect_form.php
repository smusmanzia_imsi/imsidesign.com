<?php
/**
 * @package     perfectdashboard
 * @version     1.12
 *
 * @copyright   Copyright (C) 2017 Perfect Dashboard. All rights reserved. https://perfectdashboard.com
 * @license     GNU General Public Licence http://www.gnu.org/licenses/gpl-3.0.html
 */

defined('_JEXEC') or die;

if (empty($html_tag))
{
	$html_tag = 'form';
}
?>
<<?php echo $html_tag; ?> action="<?php echo PerfectDashboard_Config::getPdUrl(); ?>site/connect?utm_source=backend&amp;utm_medium=installer&amp;utm_term=jed&amp;utm_campaign=in"
    id="perfectdashboard_connect_form" method="post" enctype="multipart/form-data" target="_blank">
    <?php include dirname(__FILE__) . '/connect_form_fields.php'; ?>
</<?php echo $html_tag; ?>>