<?php
defined('_JEXEC') or die;

class PerfectDashboard_Cms_Joomla_Task_PostCmsUpgradeDo extends PerfectDashboard_Task_PostCmsUpgradeDo
{
	/**
	 * @return array
	 */
	public function doTask()
	{
		$this->setInput('type', 'cms');
		$this->setInput('slug', 'joomla');

		return PerfectDashboard_Task::getInstance('PostCmsUpdateDo', $this->payload)
			->doTask();
	}
}