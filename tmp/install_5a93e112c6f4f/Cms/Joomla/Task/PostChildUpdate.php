<?php
defined('_JEXEC') or die;

class PerfectDashboard_Cms_Joomla_Task_PostChildUpdate extends PerfectDashboard_Task_PostChildUpdate
{
	/**
	 * @return array
	 */
	public function doTask()
	{
		$this->setInput('type', 'plugin');
		$this->setInput('slug', PERFECTDASHBOARD_J_PLUGIN_SLUG);

		// Joomla needs an URL
		$this->setInput('file_url', PerfectDashboard_Config::getPdUrl()
			. 'download/child/' . PERFECTDASHBOARD_CMS . '/perfectdashboard.zip');

		// Download package.
		PerfectDashboard_Log::debug('Download package');
		$result = PerfectDashboard_Task::getInstance('PostFileDownload', $this->payload)
			->doTask();

		if (empty($result['success']))
		{
			// Error - do not continue execution.
			return $result;
		}
		else
		{
			$this->setInput('file_path', $result['return']['file_path']);
		}

		// Unpack package.
		PerfectDashboard_Log::debug('Unpack package');
		$result = PerfectDashboard_Task::getInstance('PostFileUnpack', $this->payload)
			->doTask();

		if (empty($result['success']))
		{
			// Error - do not continue execution.
			return $result;
		}
		else
		{
			$this->setInput('path', $result['return']['path']);
		}

		// Install update.
		PerfectDashboard_Log::debug('Install package');
		return PerfectDashboard_Task::getInstance('PostExtensionUpdate', $this->payload)
			->doTask();
	}
}