<?php
defined('_JEXEC') or die;

class PerfectDashboard_Cms_Joomla_Task_PostCmsUpgradeBefore extends PerfectDashboard_Task_PostCmsUpgradeBefore
{
	/**
	 * @return array
	 */
	public function doTask()
	{
		return PerfectDashboard_Task::getInstance('PostCmsUpdateBefore', $this->payload)
			->doTask();
	}
}