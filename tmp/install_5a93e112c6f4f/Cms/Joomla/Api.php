<?php
defined('_JEXEC') or die;

class PerfectDashboard_Cms_Joomla_Api extends PerfectDashboard_Api
{
	protected $cms_initialized = false;

	protected function init()
	{
		parent::init();

		if ($this->initialized)
		{
			$this->initCms();
		}
	}

	public function initCms()
	{
		if ($this->cms_initialized)
		{
			return;
		}

		require_once PERFECTDASHBOARD_J_PLUGIN_HELPER_PATH . 'Joomla.php';

		$this->cms_initialized = true;

		if ($this->isInitialized() || defined('PERFECTDASHBOARD_J_PLUGIN_INSTALLER'))
		{
			// There is a lot of warnings during the request to J!3.0
			if (version_compare(JVERSION, '3.1.0', '<') &&
				version_compare(JVERSION, '3.0.0', '>='))
			{
				call_user_func('error' . '_report' . 'ing', 0);
			}

			// Set default language.
			/** @var \Joomla\CMS\Language\Language|JLanguage $lang */
			$lang = JFactory::getLanguage();
			$lang->setDefault('en-GB');
			$lang->setLanguage('en-GB');
			$lang->load();

			PerfectDashboard_Db::getInstance()->setDefaultDbo();

			$this->overrideJoomlaLibrary();
		}
	}

	protected function overrideJoomlaLibrary()
	{
		if (defined('PERFECTDASHBOARD_JLIB_PATH'))
		{
			return;
		}

		if (version_compare(JVERSION, '3.8.0', '>='))
		{
			//TODO there are totally new libraries
			return;
		}

		// override Joomla Libraries
		define('PERFECTDASHBOARD_JLIB_PATH', PERFECTDASHBOARD_J_PLUGIN_PATH . 'Cms/Joomla/Library/');

		JLoader::import('joomla.http.transport', PERFECTDASHBOARD_JLIB_PATH);
		JLoader::register('JHttpTransport', PERFECTDASHBOARD_JLIB_PATH . 'joomla/http/transport.php', true);

		JLoader::import('joomla.http.transport.curl', PERFECTDASHBOARD_JLIB_PATH);
		JLoader::register('JHttpTransportCurl', PERFECTDASHBOARD_JLIB_PATH . 'joomla/http/transport/curl.php', true);

		JLoader::import('joomla.http.transport.socket', PERFECTDASHBOARD_JLIB_PATH);
		JLoader::register('JHttpTransportSocket', PERFECTDASHBOARD_JLIB_PATH . 'joomla/http/transport/socket.php', true);

		JLoader::import('joomla.http.transport.stream', PERFECTDASHBOARD_JLIB_PATH);
		JLoader::register('JHttpTransportStream', PERFECTDASHBOARD_JLIB_PATH . 'joomla/http/transport/stream.php', true);

		if (version_compare(JVERSION, '2.5.15', '<'))
		{
			JLoader::import('joomla.http.factory', PERFECTDASHBOARD_JLIB_PATH);
			JLoader::register('JHttpFactory', PERFECTDASHBOARD_JLIB_PATH . 'joomla/http/factory.php', false);
		}

		if (version_compare(JVERSION, '3.0', '<'))
		{
			JLoader::import('joomla.log.logger.callback', PERFECTDASHBOARD_JLIB_PATH);
			JLoader::register('JLogLoggerCallback', PERFECTDASHBOARD_JLIB_PATH . 'joomla/log/logger/callback.php', true);
		}
	}
}