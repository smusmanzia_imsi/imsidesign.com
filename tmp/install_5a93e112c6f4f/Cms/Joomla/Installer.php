<?php
defined('_JEXEC') or die;

class PerfectDashboard_Cms_Joomla_Installer extends PerfectDashboard_Installer
{
	protected $uninstalled = false;

	public function __construct()
	{
		jimport('joomla.installer.installer');

		PerfectDashboard_Api::getInstance()->initCms();
	}

	/**
	 * @return bool
	 */
	public function install()
	{
		$result = parent::install();

		$result = $this->enableExtension() && $result;

		return $result;
	}

	/**
	 * @return bool
	 */
	public function update()
	{
		$version = PerfectDashboard_Config::get('version', '1.0');
		$result  = true;

		if (version_compare($version, '1.12', '<'))
		{
			// Enable extension if it has been updated from component to plugin
			$result = $this->enableExtension();
		}

		$result = parent::update() && $result;

		if (defined('PERFECTDASHBOARD_J_PLUGIN_HELPER_PATH'))
		{
			include_once PERFECTDASHBOARD_J_PLUGIN_HELPER_PATH . 'Joomla.php';
			if (class_exists('PerfectDashboard_Cms_Joomla_Helper_Whitelabeller'))
			{
				$labeller = new PerfectDashboard_Cms_Joomla_Helper_Whitelabeller();
				$labeller->handle();
			}
		}

		return $result;
	}

	/**
	 * @return bool
	 */
	public function uninstall()
	{
		// Make sure that it would not run it twice with Joomla Extensions Manager
		if ($this->uninstalled)
		{
			return true;
		}
		$this->uninstalled = true;

		$result = parent::uninstall();

		// Do not run the uninstall script again if it was trigger by Joomla back-end
		if (defined('PERFECTDASHBOARD_J_PLUGIN_INSTALLER'))
		{
			return $result;
		}

		try
		{
			/** @var \Joomla\CMS\Installer\Installer|JInstaller $installer */
			$installer = JInstaller::getInstance();
			$this->disableExtensionProtection();

			$result = $installer->uninstall('plugin', $this->getExtensionId()) && $result;
		}
		catch (Exception $e)
		{
			PerfectDashboard_Log::error('Failed to uninstall Child extension: ' . $e->getMessage());
			$result = false;
		}

		return $result;
	}

	/**
	 *
	 * @return int
	 */
	protected function getExtensionId()
	{
		/** @var \Joomla\Database\DatabaseDriver|JDatabaseDriver $db */
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true)
			->select($db->qn('extension_id'))
			->from($db->qn('#__extensions'))
			->where($this->getExtensionWhereCondition());

		try
		{
			return (int) $db->setQuery($query)->loadResult();
		}
		catch (Exception $e)
		{
			PerfectDashboard_Log::error('Failed to get Child extension ID: ' . $e->getMessage());
		}

		return 0;
	}

	/**
	 * @return bool
	 */
	protected function disableExtensionProtection()
	{
		PerfectDashboard_Log::debug('Disable Child extension protection');

		/** @var \Joomla\Database\DatabaseDriver|JDatabaseDriver $db */
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true)
			->update($db->qn('#__extensions'))
			->set($db->qn('protected') . ' = ' . $db->q(0, false))
			->where($this->getExtensionWhereCondition());

		try
		{
			return $db->setQuery($query)->execute();
		}
		catch (Exception $e)
		{
			PerfectDashboard_Log::error('Failed to disable Child extension protection: ' . $e->getMessage());
		}

		return false;
	}

	/**
	 * @return bool
	 */
	protected function enableExtension()
	{
		PerfectDashboard_Log::debug('Enable Child extension');

		/** @var \Joomla\Database\DatabaseDriver|JDatabaseDriver $db */
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true)
			->update($db->qn('#__extensions'))
			->set($db->qn('enabled') . ' = ' . $db->q(1, false))
			->where($this->getExtensionWhereCondition());

		try
		{
			return $db->setQuery($query)->execute();
		}
		catch (Exception $e)
		{
			PerfectDashboard_Log::error('Failed to enable Child extension: ' . $e->getMessage());
		}

		return false;
	}

	/**
	 * @return array
	 */
	protected function getExtensionWhereCondition()
	{
		return PerfectDashboard_Cms_Joomla_Helper_Joomla::getChildWhereCondition();
	}

	/**
	 * @return bool
	 */
	protected function migrateVersion1_12()
	{
		jimport('joomla.filesystem.file');

		// Load old component and configuration
		/** @var \Joomla\CMS\Table\Extension|JTableExtension $table */
		$table = JTable::getInstance('extension');
		$table->load(array(
			'type'    => 'component',
			'element' => 'com_perfectdashboard'
		));
		$config = PerfectDashboard_Cms_Joomla_Helper_Joomla::getRegistry($table->get('params'));

		$backuptool_dir = $config->get('backup_dir');
		if ($backuptool_dir)
		{
			PerfectDashboard_Config::set('backuptool_dir', $backuptool_dir);
		}

		$hide_child            = $table->get('protected');
		$whitelabel_child_page = $config->get('extensions_view_information');

		// Load white labelled name from language file
		/** @see \Joomla\CMS\Language\Language::getTag */
		$whitelabel_name   = '';
		$lang_tag          = JFactory::getLanguage()->getTag();
		$override_filename = JPATH_ADMINISTRATOR . '/language/overrides/' . $lang_tag . '.override.ini';
		if (JFile::exists($override_filename))
		{
			$override_content = file_get_contents($override_filename);
			if ($override_content)
			{
				$overrides = explode("\n", $override_content);
				foreach ($overrides as $override)
				{
					if (strpos($override, 'COM_PERFECTDASHBOARD=') === 0)
					{
						$whitelabel_name = str_replace('COM_PERFECTDASHBOARD=', '', $override);
						break;
					}
				}
			}
		}

		require_once PERFECTDASHBOARD_J_PLUGIN_HELPER_PATH . 'WhiteLabeller.php';
		$labeller = new PerfectDashboard_Cms_Joomla_Helper_Whitelabeller(
			$whitelabel_name, $whitelabel_child_page, null, $hide_child);

		if ($hide_child)
		{
			$labeller->hideChild();
		}

		if ($whitelabel_name)
		{
			$labeller->setName();
		}

		if ($whitelabel_child_page)
		{
			$labeller->setChildPage();
		}

		/** @var \Joomla\CMS\Installer\Installer|JInstaller $installer */
		$installer = JInstaller::getInstance();
		$installer->uninstall('component', (int) $table->get('extension_id'));

		return parent::migrateVersion1_12();
	}
}