<?php
defined('PERFECTDASHBOARD_LIB') or die;

class PerfectDashboard_Loader
{
	protected static $class_prefix = 'PerfectDashboard_';
	protected static $loaded = array();

	/**
	 * @return string
	 */
	public static function getClassPrefix()
	{
		return static::$class_prefix;
	}

	/**
	 * @param string $name
	 *
	 * @return bool|string
	 */
	public static function loadClass($name)
	{
		if (isset(static::$loaded[$name]))
		{
			return static::$loaded[$name];
		}

		$path = str_replace('_', '/', $name) . '.php';

		if (!file_exists(PERFECTDASHBOARD_LIB_PATH . $path))
		{
			return false;
		}

		include_once PERFECTDASHBOARD_LIB_PATH . $path;

		if (!class_exists(static::getClassPrefix() . $name))
		{
			return false;
		}

		static::$loaded[$name] = static::getClassPrefix() . $name;

		if (PERFECTDASHBOARD_CMS)
		{
			$path = dirname(dirname(PERFECTDASHBOARD_LIB_PATH)) . '/Cms/' . ucfirst(strtolower(PERFECTDASHBOARD_CMS)) . '/' . $path;
			if (file_exists($path))
			{
				include_once $path;
				$prefix = 'Cms_' . ucfirst(strtolower(PERFECTDASHBOARD_CMS)) . '_';
				if (class_exists(static::getClassPrefix() . $prefix . $name))
				{
					static::$loaded[$name] = static::$loaded[$prefix . $name] = static::getClassPrefix() . $prefix . $name;
				}
			}
		}

		return static::$loaded[$name];
	}
}