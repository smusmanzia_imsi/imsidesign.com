<?php
defined('PERFECTDASHBOARD_LIB') or die;

class PerfectDashboard_Task_PostDebugEnable extends PerfectDashboard_Task_Base
{
	/**
	 * @return array
	 */
	public function doTask()
	{
		$debug = (bool) PerfectDashboard_Config::get('debug');
		if (!$debug && PerfectDashboard_Config::set('debug', true))
		{
			$debug = true;
		}

		return array(
			'success' => ($debug === true)
		);
	}
}