<?php
defined('PERFECTDASHBOARD_LIB') or die;

class PerfectDashboard_Task_PostDebugLogs extends PerfectDashboard_Task_Base
{
	/**
	 * @return array
	 */
	public function doTask()
	{
		$date = preg_replace('/[^0-9-]/', '',
			$this->input('date', date('Y-m-d'))
		);
		$path = PerfectDashboard_Log::getInstance()->getLogsPath()
			. 'perfectdashboard_' . $date . '.logs.php';

		$filemanager = PerfectDashboard_Filemanager::getInstance();
		if (!$filemanager->is_file($path))
		{
			throw new PerfectDashboard_Exception_Response('Logs file with date: ' . $date . ' was not found', 404);
		}

		return array(
			'success' => true,
			'logs'    => $filemanager->get_contents($path),
		);
	}
}