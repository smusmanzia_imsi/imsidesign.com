<?php
defined('PERFECTDASHBOARD_LIB') or die;

class PerfectDashboard_Task_PostCmsUpdateBefore extends PerfectDashboard_Task_Base
{
	/**
	 * @return array
	 */
	public function doTask()
	{
		return array(
			'success' => true,
			'return'  => array(
				'path' => $this->input('path'),
			),
		);
	}
}