<?php
defined('PERFECTDASHBOARD_LIB') or die;

class PerfectDashboard_Task_PostCmsEnable extends PerfectDashboard_Task_Base
{
	/**
	 * @return array
	 */
	public function doTask()
	{
		$offline = (bool) PerfectDashboard_Config::get('offline');
		if ($offline && PerfectDashboard_Config::set('offline', false))
		{
			$offline = false;
		}

		return array(
			'success'    => ($offline === false),
			'is_offline' => $offline,
		);
	}
}