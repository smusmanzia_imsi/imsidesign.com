<?php
defined('PERFECTDASHBOARD_LIB') or die;

class PerfectDashboard_Task_PostBackupDownload extends PerfectDashboard_Task_Base
{
	/**
	 * @throws PerfectDashboard_Exception_Response
	 *
	 * @return array
	 */
	public function doTask()
	{
		$id       = $this->input('id', null);
		$filename = $this->input('filename', null);

		$backup = PerfectDashboard_Backuptool::getInstance()
			->getBackup($id, $filename);

		if (empty($backup['exists']))
		{
			throw new PerfectDashboard_Exception_Response('Backup file was not found', 404);
		}

		PerfectDashboard_Response::getInstance()
			->sendFile($backup['path']);
	}
}