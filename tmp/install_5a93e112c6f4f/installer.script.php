<?php
/**
 * @package     perfectdashboard
 * @version     1.12
 *
 * @copyright   Copyright (C) 2017 Perfect Dashboard. All rights reserved. https://perfectdashboard.com
 * @license     GNU General Public Licence http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die;

class plgSystemPerfectdashboardInstallerScript
{
	protected $version = '';
	protected $previous_version = '';
	protected $action = 'install';

	/**
	 * @param JAdapterInstance|\Joomla\CMS\Installer\InstallerAdapter $adapter
	 */
	protected function init($adapter)
	{
		if (!defined('PERFECTDASHBOARD_J_PLUGIN_INSTALLER'))
		{
			define('PERFECTDASHBOARD_J_PLUGIN_INSTALLER', true);
		}

		$parent         = $adapter->getParent();
		$extension_root = rtrim($parent->getPath('extension_root'), '/\\') . '/';

		$this->loadLanguage();

		require_once $extension_root . 'defines.php';
		require_once $extension_root . 'lib/src/Init.php';
		require_once PERFECTDASHBOARD_LIB_PATH . 'Installer.php';
	}

	protected function out($message, $type = 'message')
	{
		if (php_sapi_name() === 'cli')
		{
			fwrite(STDOUT, '[' . strtoupper($type) . '] ' . $message . PHP_EOL);
		}
		else
		{
			/** @see \Joomla\CMS\Application\CMSApplication::enqueueMessage */
			JFactory::getApplication()
				->enqueueMessage($message, $type);
		}
	}

	/**
	 * Called before any type of action.
	 *
	 * @param string                                                  $action  The type of change (install, uninstall, update or discover_install)
	 * @param JAdapterInstance|\Joomla\CMS\Installer\InstallerAdapter $adapter The object responsible for running this script
	 *
	 * @return bool
	 */
	public function preflight($action, $adapter)
	{
		if (version_compare(PHP_VERSION, '5.3.0', '<'))
		{
			$this->out('Perfect Dashboard requires at least PHP 5.3 version', 'error');

			return false;
		}

		if (version_compare(JVERSION, '2.5.5', '<'))
		{
			$this->out('Perfect Dashboard requires at least Joomla 2.5.5 version', 'error');

			return false;
		}

		$parent      = $adapter->getParent();
		$path_source = rtrim($parent->getPath('source'), '/\\') . '/';

		$manifest      = @simplexml_load_file($path_source . '/perfectdashboard.xml');
		$this->version = (string) $manifest->version;
		unset($manifest);

		$path_plugin    = JPATH_PLUGINS . '/system/perfectdashboard/perfectdashboard.xml';
		$path_component = JPATH_ADMINISTRATOR . '/components/com_perfectdashboard/perfectdashboard.xml';
		if (file_exists($path_plugin))
		{
			$this->action           = 'update';
			$manifest               = @simplexml_load_file($path_plugin);
			$this->previous_version = (string) $manifest->version;
			unset($manifest);
		}
		elseif (file_exists($path_component))
		{
			$manifest               = @simplexml_load_file($path_component);
			$this->previous_version = (string) $manifest->version;
			unset($manifest);
		}

		return true;
	}

	/**
	 * Called on uninstallation
	 *
	 * @param   JAdapterInstance|\Joomla\CMS\Installer\InstallerAdapter $adapter The object responsible for running this script
	 *
	 * @return  boolean  True on success
	 */
	public function uninstall($adapter)
	{
		$this->action = 'uninstall';
		$this->init($adapter);

		PerfectDashboard_Installer::getInstance()
			->uninstall();

		$this->cacheClean();

		PerfectDashboard_Log::debug('Perfect Dashboard has been uninstalled.');

		return true;
	}

	/**
	 * Called after any type of action
	 *
	 * @param   string                                                  $action  The type of change (install, uninstall, update or discover_install)
	 * @param   JAdapterInstance|\Joomla\CMS\Installer\InstallerAdapter $adapter The object responsible for running this script
	 *
	 * @return  boolean  True on success
	 */
	public function postflight($action, $adapter)
	{
		if ($action == 'uninstall')
		{
			return true;
		}

		// Do nothing on action == update. It will be finished by the task POST child/update/after or plugin self-update
		$this->init($adapter);

		$installer = PerfectDashboard_Installer::getInstance();

		// Migrate component to plugin
		if ($this->previous_version && version_compare($this->previous_version, '1.12', '<'))
		{
			PerfectDashboard_Log::debug('Component Perfect Dashboard has been found. Switch from plugin installation to update.');

			// Make the plugin to think that the older version is already installed
			PerfectDashboard_Config::set('version', $this->previous_version);

			if (!empty($_REQUEST['site_id']))
			{
				$installer->setOption('site_id', (int) $_REQUEST['site_id']);
			}

			$installer
				->setOption('version', $this->version)
				->update();
		}
		elseif ($action == 'install' || $action == 'discover_install')
		{
			$installer->install();
		}

		$this->cacheClean();

		// Do not display success message when running in CLI
		if (php_sapi_name() === 'cli')
		{
			return true;
		}

		// Extensions Developers income source
		/** @var \Joomla\CMS\Session\Session|JSession $session */
		$session = JFactory::getSession();
		if ($session->get('redirect', null, 'perfectdashboard'))
		{
			$session->set('redirect', null, 'perfectdashboard');
			$referral_key = $session->get('referral', null, 'perfectdashboard');

			$this->connectWebsite($referral_key, $this->version);
			$this->cleanupInstall($adapter->getParent()->getPath('source'));
			exit();
		}

		// Installation in back-end
		$this->displayConnectMessage();

		return true;
	}

	protected function displayConnectMessage()
	{
		ob_start();
		include PERFECTDASHBOARD_J_PLUGIN_PATH . 'layouts/connect_message.php';
		$message = ob_get_clean();

		/** @see \Joomla\CMS\Application\CMSApplication::enqueueMessage */
		JFactory::getApplication()->enqueueMessage($message, 'message');
	}

	/**
	 * @param string $referral_key Used in the template file
	 */
	protected function connectWebsite($referral_key, $version = null)
	{
		include PERFECTDASHBOARD_J_PLUGIN_PATH . 'layouts/connect_ed.php';
	}

	protected function cacheClean()
	{
		/* @see \Joomla\CMS\Factory::getConfig */
		$config = JFactory::getConfig();

		// Check if cache is enabled.
		if ($config->get('caching'))
		{
			/** @var \Joomla\CMS\Cache\Cache|JCache $cache */
			$cache = JFactory::getCache();
			$cache->clean('_system', 'group');
		}
	}

	/**
	 * @param string $extractdir
	 */
	protected function cleanupInstall($extractdir)
	{
		if (is_dir($extractdir))
		{
			JFolder::delete($extractdir);
		}

		$files = JFolder::files(dirname($extractdir), 'perfect.*dashboard.*\.zip', false, true);
		if (is_array($files) && count($files))
		{
			JFile::delete($files);
		}
	}

	protected function loadLanguage()
	{
		/* @see \Joomla\CMS\Language\Language::load */
		return JFactory::getLanguage()
			->load('plg_system_perfectdashboard', JPATH_ADMINISTRATOR, null, true, true);
	}
}