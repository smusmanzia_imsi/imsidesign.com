<?php
defined('_JEXEC') or die;

if (!defined('PERFECTDASHBOARD_LIB'))
{
	$manifest = simplexml_load_file(dirname(__FILE__) . '/perfectdashboard.xml');

	define('PERFECTDASHBOARD_STAGE', 'app');

	define('PERFECTDASHBOARD_LIB', true);
	define('PERFECTDASHBOARD_CMS', 'joomla');
	define('PERFECTDASHBOARD_SITE_PATH', rtrim(JPATH_ROOT, '/\\') . '/');
	if (!defined('PERFECTDASHBOARD_VERSION'))
	{
		define('PERFECTDASHBOARD_VERSION', (string) $manifest->version);
	}

	define('PERFECTDASHBOARD_J_PLUGIN_PATH', JPATH_PLUGINS . '/system/perfectdashboard/');
	define('PERFECTDASHBOARD_J_PLUGIN_SLUG', (string) $manifest->files->filename['plugin']);
	define('PERFECTDASHBOARD_J_PLUGIN_HELPER_PATH', PERFECTDASHBOARD_J_PLUGIN_PATH . 'Cms/Joomla/Helper/');
}