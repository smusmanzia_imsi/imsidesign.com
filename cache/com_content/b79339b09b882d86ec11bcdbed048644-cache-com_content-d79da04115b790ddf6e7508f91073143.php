<?php die("Access Denied"); ?>#x#a:5:{s:4:"body";s:10649:"<article class="item item-page" itemscope itemtype="http://schema.org/Article">
	<meta itemprop="inLanguage" content="en-US" />
		
		<div class="entry-image full-image"> <img
				src="images/blog/new-turbocad-expert-2017-cad-for-advanced-users.png" alt="" itemprop="image"/> </div>
        
	<div class="entry-header has-post-format">
		                            					            <span class="post-format"><i style="margin-right:-6px;" class="fa fa-pencil-square-o"></i></span>
                                        <h2 itemprop="name">
                                            IMSI Design Introduces TurboCAD Expert 2017                                    </h2>
             
            
            	<dl class="article-info">

		
			<dt class="article-info-term"></dt>	
				
							<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
	<i class="fa fa-user"></i>
					<span itemprop="name" data-toggle="tooltip" title="Written by ">IMSI Design</span>	</dd>			
			
							<dd class="category-name">
	<i class="fa fa-folder-open-o"></i>
				<span itemprop="genre" itemprop="genre" data-toggle="tooltip" title="Article Category">Press Releases</span>	</dd>			
			
							<dd class="published">
	<i class="fa fa-calendar-o"></i>
	<time datetime="2017-04-12T17:03:00+00:00" itemprop="datePublished" data-toggle="tooltip" title="Published Date">
		12 April 2017	</time>
</dd>			
		
					
			
					
		
	</dl>
  
        	</div>

	    
	
				
			<div itemprop="articleBody">
		
<h3 class="press-release-sub-title">New Product Part of a More Streamlined TurboCAD Product Line</h3>
<p>NOVATO, Calif., April 12, 2017 – IMSI® Design, a leading CAD software developer, today announced a new addition to its TurboCAD product line titled TurboCAD Expert 2017, a 2D/3D CAD application for Windows® desktop PCs.</p>
<p>TurboCAD Expert is for experienced 2D/3D CAD users already familiar with AutoCAD or AutoCAD LT looking for a powerful alternative. It offers all of the 2D drafting and design tools users require, with an optional AutoCAD®-like 2D drafting interface, complete with command line and dynamic input cursor, which simplifies the transition from AutoCAD. TurboCAD Expert also includes the easy to use 2D and 3D design tools common to TurboCAD, as well as many advanced features that CAD professionals want, including 2D geometric and dimensional constraints, a collection of architectural design tools, 3D surface modeling tools for mechanical design, photorealistic rendering features, database connectivity with customizable reporting, and support for dozens of industry standard CAD and graphic file formats.</p>
<p>“The addition of TurboCAD Expert is part of the company’s plan to streamline our product offerings and more clearly define each product’s position in the marketplace,” explains Bob Mayer, President of IMSI Design. “With the release of TurboCAD Pro Platinum 2017, we combined our TurboCAD Pro and TurboCAD Pro Platinum products into one high-end professional product,” Mayer continued. “Now TurboCAD Expert replaces our TurboCAD LTE and TurboCAD LTE Pro products as an affordable AutoCAD alternative, and acts as a stepping stone for TurboCAD Deluxe users looking for more advanced design features and additional file interoperability.”</p>
<h4>Key Features in TurboCAD Expert 2017</h4>
<ul>
<li>Complete set of 2D/3D Design Tools</li>
<li>AutoCAD®-like 2D drafting interface option</li>
<li>Command Line with Dynamic Input Cursor</li>
<li>PDF Underlay</li>
<li>Arrow Tools</li>
<li>2D Geometric and Dimension Constraints</li>
<li>Surface Modeling Tools</li>
<li>Intelligent (Parametric) Attribute-rich, Architectural Objects</li>
<li>Architectural Design and Documentation Tools</li>
<li>Photorealistic Rendering, materials, and lighting</li>
<li>Database Connectivity with Customizable Reporting</li>
<li>Point Cloud Support</li>
<li>AutoCAD (DXF, DWG, and DWF) File Compatibility</li>
<li>Enhanced SketchUp File (SKP) Import</li>
<li>3D Printer Support via .STL Import and Export</li>
<li>Import and Open of 25 file formats</li>
<li>Export, Save As, and Publish 27 file formats</li>
</ul>
<h4>Features New to TurboCAD in 2017 Include:</h4>
<ul>
<li>Image Management Palette</li>
<li>Table Editing / Attachable Data</li>
<li>Timestamp</li>
<li>Centerline and Center Mark</li>
<li>Relative Angle Field</li>
<li>Scale by Two Points</li>
<li>Intelligent Scaling</li>
<li>Fit to Scale for Door and Window Custom Blocks</li>
<li>House Builder Wizard</li>
<li>Roof Slab Openings</li>
<li>Enhanced Photo-realistic Shaders</li>
<li>Geo-located Sun Support</li>
<li>Caustics for Sun Lights</li>
<li>Physical Sky Lighting</li>
<li>Volumetric Effects</li>
<li>Improved AutoCAD (DXF, DWG, and DWF)</li>
<li>3D PDF (.U3D) Export</li>
<li>Google Earth (KML and KMZ) File Support</li>
<li>Point Cloud (PCD, PCG, XYZ, ASC) File Support</li>
<li>TurboApps (TAP) file import from mobile devices</li>
</ul>
<h3>Pricing &amp; Availability</h3>
<p>TurboCAD Expert 2017 is now available for $499.99 USD for a full, permanent license.</p>
<p>For more information, download a 30-day trial, or to upgrade online, please visit www.TurboCAD.com or call IMSI Design at 1.800.833.8082 (+1.415.483.8000).</p>
<h3>About IMSI Design</h3>
<p>IMSI Design is a leader in affordable, general-purpose CAD (Computer Aided Design), and a pioneer in mobile solutions for the AEC (Architectural, Engineering, and Construction) industry. With over 16 million products distributed since 1988, IMSI Design products include the TurboCAD®, DesignCAD™, TurboFloorPlan®, TurboViewer®, TurboReview®, and TurboSite® families of precision design applications for desktop and mobile. Please visit <a href="http://www.IMSIdesign.com">www.IMSIdesign.com</a> for more information.</p>
<p><strong>Contact:</strong></p>
<p>IMSI Design<br /> Pam Volpe<br /> 415.483.8025<br /> <span id="cloak9ec2bae250d433e72fb3bef1cc0a606c">JLIB_HTML_CLOAKING</span><script type='text/javascript'>
				document.getElementById('cloak9ec2bae250d433e72fb3bef1cc0a606c').innerHTML = '';
				var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
				var path = 'hr' + 'ef' + '=';
				var addy9ec2bae250d433e72fb3bef1cc0a606c = 'pr' + '&#64;';
				addy9ec2bae250d433e72fb3bef1cc0a606c = addy9ec2bae250d433e72fb3bef1cc0a606c + '&#105;ms&#105;d&#101;s&#105;gn' + '&#46;' + 'c&#111;m';
				var addy_text9ec2bae250d433e72fb3bef1cc0a606c = 'pr' + '&#64;' + '&#105;ms&#105;d&#101;s&#105;gn' + '&#46;' + 'c&#111;m';document.getElementById('cloak9ec2bae250d433e72fb3bef1cc0a606c').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy9ec2bae250d433e72fb3bef1cc0a606c + '\'>'+addy_text9ec2bae250d433e72fb3bef1cc0a606c+'<\/a>';
		</script></p>
<p>###</p>
<p>© 2017 IMSI Design, LLC. All rights reserved. IMSI and TurboCAD are registered trademarks and/or trademarks of IMSI Design, LLC. AutoCAD is a registered trademark of Autodesk. All other identifiable marks are the property of their respective owners.</p>	</div>

	    
    	<div class="helix-social-share">
		<div class="helix-social-share-blog helix-social-share-article">
			<ul>
								<li>
					<div class="facebook" data-toggle="tooltip" data-placement="top" title="Share On Facebook">

						<a class="facebook" onClick="window.open('http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-expert-2017','Facebook','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-expert-2017">
							<i class="fa fa-facebook-square"></i> Facebook						</a>

					</div>
				</li>
                								<li>
					<div class="twitter" data-toggle="tooltip" data-placement="top" title="Share On Twitter">
						<a class="twitter" onClick="window.open('http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-expert-2017&amp;text=IMSI Design Introduces TurboCAD Expert 2017','Twitter share','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-expert-2017&amp;text=IMSI%20Design%20Introduces%20TurboCAD%20Expert%202017">
							<i class="fa fa-twitter-square"></i> Twitter						</a>
					</div>
				</li>
                                				<li>
					<div class="google-plus">
						<a class="google-plus" data-toggle="tooltip" data-placement="top" title="Share On Google Plus" onClick="window.open('https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-expert-2017','Google plus','width=585,height=666,left='+(screen.availWidth/2-292)+',top='+(screen.availHeight/2-333)+''); return false;" href="https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-expert-2017" >
						<i class="fa fa-google-plus"></i></a>
					</div>
				</li>
				                				<li>
					<div class="linkedin">
						<a class="linkedin" data-toggle="tooltip" data-placement="top" title="Share On Linkedin" onClick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-expert-2017','Linkedin','width=585,height=666,left='+(screen.availWidth/2-292)+',top='+(screen.availHeight/2-333)+''); return false;" href="http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-expert-2017" >	
						<i class="fa fa-linkedin-square"></i></a>
					</div>
				</li>
                			</ul>
		</div>		
	</div> <!-- /.helix-social-share -->
    	<div style="margin:0 auto 35px;" class="clearfix"></div><hr />
        
        			
	
<nav role="pagination">
    <ul class="cd-pagination no-space animated-buttons custom-icons">
		            <li class="button btn-previous">
                <a href="/us/blog/press-releases/imsi-design-releases-turbocad-deluxe-2017" rel="prev"><i>Prev</i></a>
            </li>
                
                   <li class="button btn-next">
                <a href="/us/blog/press-releases/imsi-design-introduces-turbocad-pro-platinum-2017" rel="next"><i>Next</i></a>
            </li>
            </ul>
</nav>
				    
    
	</article>";s:4:"head";a:11:{s:5:"title";s:25:"IMSI Design - IMSI Design";s:11:"description";s:219:"IMSI Design is a leader in affordable, general-purpose CAD (Computer Aided Design) and home design desktop software, and a pioneer in mobile solutions for the AEC (Architectural, Engineering, and Construction) industry.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:4:"name";a:3:{s:8:"keywords";s:39:"IMSI Design, CAD, TurboCAD, home design";s:6:"rights";N;s:6:"author";s:11:"IMSI Design";}}s:5:"links";a:0:{}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:0:{}s:6:"script";a:0:{}s:6:"custom";a:7:{i:0;s:136:"<meta property="og:url" content="http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-expert-2017" />";i:1;s:45:"<meta property="og:type" content="article" />";i:2;s:82:"<meta property="og:title" content="IMSI Design Introduces TurboCAD Expert 2017" />";i:3;s:206:"<meta property="og:description" content="NOVATO, Calif., April 12, 2017 – IMSI® Design, a leading CAD software developer, today announced a new addition to its TurboCAD product line titled TurboCA..." />";i:4;s:135:"<meta property="og:image" content="http://test.imsitechnologies.com/images/blog/new-turbocad-expert-2017-cad-for-advanced-users.png" />";i:5;s:48:"<meta property="og:image:width" content="600" />";i:6;s:49:"<meta property="og:image:height" content="315" />";}s:10:"scriptText";a:0:{}}s:13:"mime_encoding";s:9:"text/html";s:7:"pathway";a:0:{}s:6:"module";a:0:{}}