<?php die("Access Denied"); ?>#x#a:5:{s:4:"body";s:12020:"<article class="item item-page" itemscope itemtype="http://schema.org/Article">
	<meta itemprop="inLanguage" content="en-US" />
		
		<div class="entry-image full-image"> <img
				src="images/Case-Studies/electronic-engineering-consultant-expands-into-mechanical-cad-turbocad-case-study.png" alt="" itemprop="image"/> </div>
        
	<div class="entry-header has-post-format">
		                            					            <span class="post-format"><i style="margin-right:-6px;" class="fa fa-pencil-square-o"></i></span>
                                        <h2 itemprop="name">
                                            TurboCAD Helps Electronic Engineering Consultant Expand into Mechanical CAD                                    </h2>
             
            
            	<dl class="article-info">

		
			<dt class="article-info-term"></dt>	
				
							<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
	<i class="fa fa-user"></i>
					<span itemprop="name" data-toggle="tooltip" title="Written by ">IMSI Design</span>	</dd>			
			
							<dd class="category-name">
	<i class="fa fa-folder-open-o"></i>
				<span itemprop="genre" itemprop="genre" data-toggle="tooltip" title="Article Category">Case Studies</span>	</dd>			
			
							<dd class="published">
	<i class="fa fa-calendar-o"></i>
	<time datetime="2017-05-18T20:11:07+00:00" itemprop="datePublished" data-toggle="tooltip" title="Published Date">
		18 May 2017	</time>
</dd>			
		
					
			
					
		
	</dl>
  
        	</div>

	    
	
				
			<div itemprop="articleBody">
		
<blockquote>TurboCAD’s functionality is comparable to CAD software selling – or renting – for much more money – Jeff Lindstrom, CEO &amp; CAD Specialist, Arvak Technologies</blockquote>
<h4>Background:</h4>
<p>Jeff Lindstrom, CEO of Arvak Technologies, was originally an Electronic Engineering Consultant for small aerospace companies in Southern California, and founded Arvak Technologies in order to pursue business opportunities with the Defense Logistics Agency (DLA). Since then, Arvak Technologies’ business model has expanded to include web-based freelancing.</p>
<h4>Challenges:</h4>
<p>Jeff Lindstrom had used CorelDraw for nearly ten years when he decided to expand into Mechanical CAD to improve his usefulness to small aerospace companies that lacked internal CAD resources. This was an area where CorelDraw had no substantive capabilities. "I was able to design a chassis for a large Acceptance Test Interface project that is still in use today," Lindstrom remembers, "However, it was only 2D, and any draft views I created were done manually." Of course, this manual work, using software that lacked the required tools and capabilities for the job, was very labor intensive, and Lindstrom started looking for an alternative CAD solution.</p>
<h4>Product Search:</h4>
<p>When starting the search for a CAD product more suitable for the type of mechanical CAD work that he wanted to do, Jeff Lindstrom’s two main criteria were capabilities and cost. He looked at a few different products, including AutoCAD, and says: "AutoCAD was far too expensive to justify the amount of work I expected to do with it." A CAD specialist at a client company, an AutoCAD user, suggested TurboCAD, and after some research, it seemed to be just what Lindstrom had been looking for. "TurboCAD provided all the features I needed, and then some," he says.</p>
<blockquote>
<p>AutoCAD was far too expensive to justify the amount of work I expected to do with it.</p>
</blockquote>
<h4>Implementation &amp; Product Use:</h4>
<p>A TurboCAD user since 2007, Jeff Lindstrom is very familiar with the ins and outs of TurboCAD; however, as with all CAD software, there were a few initial hurdles to overcome. "As seems common with CAD software, the user interface was very confusing at first," he remembers. However, Lindstrom solved the problem by getting tutorials for both the 2D and the 3D tools, as well as one that covered an entire CAD project. He has also had a lot of help from the TurboCAD User Forum over the years. "The User Forum is robust," he says, "with help freely offered by a variety of experts."</p>
<p>Jeff Lindstrom now uses TurboCAD Pro Platinum both in his work for the Defense Department, and for freelance CAD projects. For the Defense Department work, which is build-to-print, Lindstrom transcribes existing mechanical and assembly drawings into 3D, in order to get online quotes for manufacturing at JCP- or ITAR-approved facilities.</p>
<blockquote>
<p>TurboCAD's photorealistic rendering capabilities enhance presentations by providing eye-catching visuals for our proposals.</p>
</blockquote>
<p>As far as freelance projects go, a recent CAD project was for a client who needed a new lower chamber to replace the original on a commercial humidifier. Requirements included the abilities to hold a micro-controller assembly, a fan, and a battery pack. With the help of TurboCAD, Lindstrom was able to quickly create a design that met the client’s needs and requirements. "I selected a fan that met their operational requirements, took a few measurements, and designed the chamber in short order," he explains. The next step was adding the struts that allowed the chamber to be mated with the humidifier’s water basin. Finally, Lindstrom rendered the pieces as an assembled unit, as well as with an exploded view for client documentation. "It took longer for me to decide what to do than it took for me to do it with TurboCAD Pro Platinum's Solid Modeling tools," he says.</p>
<p>For the types of work that Arvak Technologies does, several TurboCAD tools and features have come in handy over the years. "I most often use the ACIS Solid Modeling tools," Jeff Lindstrom says. "I especially like the Quick Pull, Extrude, and Lofting functions, plus Chamfer and Fillet." He also finds TurboCAD’s 2D tools very useful for creating the guidelines for modifications. Photorealistic Rendering capabilities should also be mentioned. Lindstrom explains: "TurboCAD's photorealistic rendering capabilities enhance presentations by providing eye-catching visuals for our proposals."</p>
<h4>Results &amp; Benefits:</h4>
<p>Since Jeff Lindstrom first came in contact with TurboCAD in 2007, the software has become a crucial and highly beneficial part of his day-to-day work – and a profitable one at that. With the help of TurboCAD, Arvak Technologies has been able to carry out numerous projects, both Defense Department-related and freelance. To this day, Lindstrom keeps developing his own CAD knowledge and capabilities with each project. "Figuring out how to create an object improves my ability to conceive better structures on subsequent projects," he says.</p>
<blockquote>
<p>TurboCAD's lease price is among the lowest in the marketplace, and the software can still be purchased outright, something that is becoming all too uncommon among its competitors.</p>
</blockquote>
<p>Moving forward, Jeff Lindstrom wants to take on the challenge of learning the TurboCAD Software Development Kit (SDK), in order to support the design of future custom functionality. "I would like to provide plug-ins or add-ons that enhance the functionality of TurboCAD based on tasks that took longer to complete than I would have liked," he says.</p>
<p>To others considering TurboCAD Pro Platinum, Jeff Lindstrom says: "TurboCAD’s functionality is comparable to CAD software selling – or renting – for much more money. Its File Import/Export capabilities provide the widest level of interoperability with other CAD installations." He also really likes how the user interface can be re-organized to maximize performance for each user.</p>
<p>Finally, Jeff Lindstrom brings up software leasing, a system that is becoming very common in the CAD industry, as well as in other design-related fields. "TurboCAD's lease price is among the lowest in the marketplace, and the software can still be purchased outright, something that is becoming all too uncommon among its competitors."</p>	</div>

	    
    	<div class="helix-social-share">
		<div class="helix-social-share-blog helix-social-share-article">
			<ul>
								<li>
					<div class="facebook" data-toggle="tooltip" data-placement="top" title="Share On Facebook">

						<a class="facebook" onClick="window.open('http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-helps-electronic-engineering-consultant-expand-into-mechanical-cad','Facebook','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-helps-electronic-engineering-consultant-expand-into-mechanical-cad">
							<i class="fa fa-facebook-square"></i> Facebook						</a>

					</div>
				</li>
                								<li>
					<div class="twitter" data-toggle="tooltip" data-placement="top" title="Share On Twitter">
						<a class="twitter" onClick="window.open('http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-helps-electronic-engineering-consultant-expand-into-mechanical-cad&amp;text=TurboCAD Helps Electronic Engineering Consultant Expand into Mechanical CAD','Twitter share','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-helps-electronic-engineering-consultant-expand-into-mechanical-cad&amp;text=TurboCAD%20Helps%20Electronic%20Engineering%20Consultant%20Expand%20into%20Mechanical%20CAD">
							<i class="fa fa-twitter-square"></i> Twitter						</a>
					</div>
				</li>
                                				<li>
					<div class="google-plus">
						<a class="google-plus" data-toggle="tooltip" data-placement="top" title="Share On Google Plus" onClick="window.open('https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-helps-electronic-engineering-consultant-expand-into-mechanical-cad','Google plus','width=585,height=666,left='+(screen.availWidth/2-292)+',top='+(screen.availHeight/2-333)+''); return false;" href="https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-helps-electronic-engineering-consultant-expand-into-mechanical-cad" >
						<i class="fa fa-google-plus"></i></a>
					</div>
				</li>
				                				<li>
					<div class="linkedin">
						<a class="linkedin" data-toggle="tooltip" data-placement="top" title="Share On Linkedin" onClick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-helps-electronic-engineering-consultant-expand-into-mechanical-cad','Linkedin','width=585,height=666,left='+(screen.availWidth/2-292)+',top='+(screen.availHeight/2-333)+''); return false;" href="http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-helps-electronic-engineering-consultant-expand-into-mechanical-cad" >	
						<i class="fa fa-linkedin-square"></i></a>
					</div>
				</li>
                			</ul>
		</div>		
	</div> <!-- /.helix-social-share -->
    	<div style="margin:0 auto 35px;" class="clearfix"></div><hr />
        
        			
	
<nav role="pagination">
    <ul class="cd-pagination no-space animated-buttons custom-icons">
		            <li class="button btn-previous">
                <a href="/us/blog/case-studies/turbocad-increases-productivity-in-theater-set-design" rel="prev"><i>Prev</i></a>
            </li>
                
                   <li class="button btn-next">
                <a href="/us/blog/case-studies/the-use-of-turbocad-in-conceptual-design-product-visualization" rel="next"><i>Next</i></a>
            </li>
            </ul>
</nav>
				    
    
	</article>";s:4:"head";a:11:{s:5:"title";s:25:"IMSI Design - IMSI Design";s:11:"description";s:219:"IMSI Design is a leader in affordable, general-purpose CAD (Computer Aided Design) and home design desktop software, and a pioneer in mobile solutions for the AEC (Architectural, Engineering, and Construction) industry.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:4:"name";a:3:{s:8:"keywords";s:39:"IMSI Design, CAD, TurboCAD, home design";s:6:"rights";N;s:6:"author";s:11:"IMSI Design";}}s:5:"links";a:0:{}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:0:{}s:6:"script";a:0:{}s:6:"custom";a:7:{i:0;s:166:"<meta property="og:url" content="http://test.imsitechnologies.com/us/blog/case-studies/turbocad-helps-electronic-engineering-consultant-expand-into-mechanical-cad" />";i:1;s:45:"<meta property="og:type" content="article" />";i:2;s:114:"<meta property="og:title" content="TurboCAD Helps Electronic Engineering Consultant Expand into Mechanical CAD" />";i:3;s:203:"<meta property="og:description" content="Jeff Lindstrom, CEO of Arvak Technologies, was originally an Electronic Engineering Consultant for small aerospace companies in Southern California, and fo..." />";i:4;s:177:"<meta property="og:image" content="http://test.imsitechnologies.com/images/Case-Studies/electronic-engineering-consultant-expands-into-mechanical-cad-turbocad-case-study.png" />";i:5;s:48:"<meta property="og:image:width" content="600" />";i:6;s:49:"<meta property="og:image:height" content="315" />";}s:10:"scriptText";a:0:{}}s:13:"mime_encoding";s:9:"text/html";s:7:"pathway";a:0:{}s:6:"module";a:0:{}}