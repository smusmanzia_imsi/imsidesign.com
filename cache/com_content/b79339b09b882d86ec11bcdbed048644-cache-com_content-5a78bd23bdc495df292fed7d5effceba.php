<?php die("Access Denied"); ?>#x#a:5:{s:4:"body";s:26639:"<div class="blog" itemscope itemtype="http://schema.org/Blog">
	
	
	
	
	
				<div class="items-leading clearfix">
							<article class="item leading-0"
					itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting">
					


<div class=" has-post-format-masonry">
	    <span class="post-format-masonry"><i style="margin-right:-6px;" class="fa fa-pencil-square-o"></i></span>
     
</div>




<div class="entry-image intro-image">
			<a href="/us/blog/case-studies/use-of-designcad-in-custom-hardware-software-production">
	
	<img
		src="images/Case-Studies/custom-hardware-and-software-production-designcad-case-study.png" alt="" itemprop="thumbnailUrl"/>

			</a>
	</div>
<!-- START Post-intro --><div class="post_intro">
<div class="entry-header">
		
        					<h2 itemprop="name">
									<a href="/us/blog/case-studies/use-of-designcad-in-custom-hardware-software-production" itemprop="url">
					Use of DesignCAD in Custom Hardware &amp; Software Production</a>
							</h2>
								        	<dl class="article-info">

		
			<dt class="article-info-term"></dt>	
				
							<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
	<i class="fa fa-user"></i>
					<span itemprop="name" data-toggle="tooltip" title="Written by ">IMSI Design</span>	</dd>			
			
							<dd class="category-name">
	<i class="fa fa-folder-open-o"></i>
				<span itemprop="genre" itemprop="genre" data-toggle="tooltip" title="Article Category">Case Studies</span>	</dd>			
			
							<dd class="published">
	<i class="fa fa-calendar-o"></i>
	<time datetime="2017-05-19T16:13:58+00:00" itemprop="datePublished" data-toggle="tooltip" title="Published Date">
		19 May 2017	</time>
</dd>			
		
					
			
					
		
	</dl>
    </div>


		
<p>Alpha Omega Computer Systems, Inc. is a provider of custom hardware and software solutions for everything from oceanography, to satellite communications, to terrestrial RF networks, and anything in between and beyond. Since its establishment in 1977, the company has created products for basically every industrial and natural environment, and IMSI Design’s DesignCAD software has been used in more than 300 hardware and electronic design projects.</p>
    <div class="clearfix"></div>	
 
	
<div class="readmore">
	<a class="btn btn-readmore" href="/us/blog/case-studies/use-of-designcad-in-custom-hardware-software-production" itemprop="url">
		Read More	</a>
</div>
	    <div class="helix-social-share">
        <div class="helix-social-share-blog">
            <ul>
            	                <li>
                    <div class="facebook" data-toggle="tooltip" data-placement="top" title="Share On Facebook">

                        <a class="facebook" onClick="window.open('http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/case-studies/use-of-designcad-in-custom-hardware-software-production', 'Facebook', 'width=600,height=300,left=' + (screen.availWidth / 2 - 300) + ',top=' + (screen.availHeight / 2 - 150) + ''); return false;" href="http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/case-studies/use-of-designcad-in-custom-hardware-software-production">
                            <i class="fa fa-facebook"></i>
                        </a>

                    </div>
                </li>
                				                <li>
                    <div class="twitter"  data-toggle="tooltip" data-placement="top" title="Share On Twitter">
                        <a class="twitter" onClick="window.open('http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/use-of-designcad-in-custom-hardware-software-production&amp;text=Use%20of%20DesignCAD%20in%20Custom%20Hardware%20&%20Software%20Production', 'Twitter share', 'width=600,height=300,left=' + (screen.availWidth / 2 - 300) + ',top=' + (screen.availHeight / 2 - 150) + ''); return false;" href="http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/use-of-designcad-in-custom-hardware-software-production&amp;text=Use of DesignCAD in Custom Hardware & Software Production">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </div>
                </li>
                                                <li>
                    <div class="google-plus" data-toggle="tooltip" data-placement="top" title="Share On Google Plus">
                        <a class="google-plus" onClick="window.open('https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/use-of-designcad-in-custom-hardware-software-production', 'Google plus', 'width=585,height=666,left=' + (screen.availWidth / 2 - 292) + ',top=' + (screen.availHeight / 2 - 333) + ''); return false;" href="https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/use-of-designcad-in-custom-hardware-software-production" >
                            <i class="fa fa-google-plus"></i></a>
                    </div>
                </li>
                                                <li>
                    <div class="linkedin" data-toggle="tooltip" data-placement="top" title="Share On Linkedin" >
                        <a class="linkedin" onClick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/case-studies/use-of-designcad-in-custom-hardware-software-production', 'Linkedin', 'width=585,height=666,left=' + (screen.availWidth / 2 - 292) + ',top=' + (screen.availHeight / 2 - 333) + ''); return false;" href="http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/case-studies/use-of-designcad-in-custom-hardware-software-production" >
                            <i class="fa fa-linkedin"></i></a>
                    </div>
                </li>
                            </ul>
        </div>
    </div> <!-- /.helix-social-share -->
    

	<div class="clearfix"></div>	
	          	<div class="clearfix"></div>
	    	    	<hr class="blog_hr" />
                </div><!-- END Post-intro -->
     

				</article><hr />
									</div><!-- end items-leading -->
	
		    
                                    <div class="items-row items-masonry row clearfix">
                                                                                <div class="col-xs-12 col-sm-6 col-md-6 masonry_item">
                        <article class="item column-1"
                            itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting">
                            


<div class=" has-post-format-masonry">
	    <span class="post-format-masonry"><i style="margin-right:-6px;" class="fa fa-pencil-square-o"></i></span>
     
</div>




<div class="entry-image intro-image">
			<a href="/us/blog/case-studies/turbocad-increases-productivity-in-theater-set-design">
	
	<img
		src="images/Case-Studies/increased-productivity-in-theater-set-design-turbocad-case-study.png" alt="" itemprop="thumbnailUrl"/>

			</a>
	</div>
<!-- START Post-intro --><div class="post_intro">
<div class="entry-header">
		
        					<h2 itemprop="name">
									<a href="/us/blog/case-studies/turbocad-increases-productivity-in-theater-set-design" itemprop="url">
					TurboCAD Increases Productivity in Theater Set Design</a>
							</h2>
								        	<dl class="article-info">

		
			<dt class="article-info-term"></dt>	
				
							<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
	<i class="fa fa-user"></i>
					<span itemprop="name" data-toggle="tooltip" title="Written by ">IMSI Design</span>	</dd>			
			
							<dd class="category-name">
	<i class="fa fa-folder-open-o"></i>
				<span itemprop="genre" itemprop="genre" data-toggle="tooltip" title="Article Category">Case Studies</span>	</dd>			
			
							<dd class="published">
	<i class="fa fa-calendar-o"></i>
	<time datetime="2017-05-18T20:30:03+00:00" itemprop="datePublished" data-toggle="tooltip" title="Published Date">
		18 May 2017	</time>
</dd>			
		
					
			
					
		
	</dl>
    </div>


		
<p>The Omaha Community Playhouse is a performing arts organization in Omaha, Nebraska. Technical Director Don Hook has used TurboCAD for many years, to product 2D drawings for set pieces, and has seen an increase in productivity.</p>
    <div class="clearfix"></div>	
 
	
<div class="readmore">
	<a class="btn btn-readmore" href="/us/blog/case-studies/turbocad-increases-productivity-in-theater-set-design" itemprop="url">
		Read More	</a>
</div>
	    <div class="helix-social-share">
        <div class="helix-social-share-blog">
            <ul>
            	                <li>
                    <div class="facebook" data-toggle="tooltip" data-placement="top" title="Share On Facebook">

                        <a class="facebook" onClick="window.open('http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-increases-productivity-in-theater-set-design', 'Facebook', 'width=600,height=300,left=' + (screen.availWidth / 2 - 300) + ',top=' + (screen.availHeight / 2 - 150) + ''); return false;" href="http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-increases-productivity-in-theater-set-design">
                            <i class="fa fa-facebook"></i>
                        </a>

                    </div>
                </li>
                				                <li>
                    <div class="twitter"  data-toggle="tooltip" data-placement="top" title="Share On Twitter">
                        <a class="twitter" onClick="window.open('http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-increases-productivity-in-theater-set-design&amp;text=TurboCAD%20Increases%20Productivity%20in%20Theater%20Set%20Design', 'Twitter share', 'width=600,height=300,left=' + (screen.availWidth / 2 - 300) + ',top=' + (screen.availHeight / 2 - 150) + ''); return false;" href="http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-increases-productivity-in-theater-set-design&amp;text=TurboCAD Increases Productivity in Theater Set Design">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </div>
                </li>
                                                <li>
                    <div class="google-plus" data-toggle="tooltip" data-placement="top" title="Share On Google Plus">
                        <a class="google-plus" onClick="window.open('https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-increases-productivity-in-theater-set-design', 'Google plus', 'width=585,height=666,left=' + (screen.availWidth / 2 - 292) + ',top=' + (screen.availHeight / 2 - 333) + ''); return false;" href="https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-increases-productivity-in-theater-set-design" >
                            <i class="fa fa-google-plus"></i></a>
                    </div>
                </li>
                                                <li>
                    <div class="linkedin" data-toggle="tooltip" data-placement="top" title="Share On Linkedin" >
                        <a class="linkedin" onClick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-increases-productivity-in-theater-set-design', 'Linkedin', 'width=585,height=666,left=' + (screen.availWidth / 2 - 292) + ',top=' + (screen.availHeight / 2 - 333) + ''); return false;" href="http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-increases-productivity-in-theater-set-design" >
                            <i class="fa fa-linkedin"></i></a>
                    </div>
                </li>
                            </ul>
        </div>
    </div> <!-- /.helix-social-share -->
    

	<div class="clearfix"></div>	
	          	<div class="clearfix"></div>
	    	    	<hr class="blog_hr" />
                </div><!-- END Post-intro -->
     

                        </article>
                        <!-- end item -->
                                            </div><!-- end col-sm-* -->
                                                                                                <div class="col-xs-12 col-sm-6 col-md-6 masonry_item">
                        <article class="item column-2"
                            itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting">
                            


<div class=" has-post-format-masonry">
	    <span class="post-format-masonry"><i class="fa fa-picture-o"></i></span>
     
</div>


<div class="entry-gallery"><ul id="post-slider-775" class="list-unstyled post-slides"><li class="item active"><img src="images/2017/05/18/conceptual-design-product-visualization-turbocad-case-study.png" alt="IMSI Design"></li><li class="item"><img src="images/2017/05/18/revvll-one-concept-design-turbocad-kras-design.png" alt="IMSI Design"></li><li class="item"><img src="images/2017/05/18/conceptual-design-real-life-design-turbocad-kras-design.png" alt="IMSI Design"></li><li class="item"><img src="images/2017/05/18/concept-design-nexus-fitness-equipment-vs-reality-kras-design-turbocad.png" alt="IMSI Design"></li></ul></div><script type="text/javascript">
 jQuery(function($){
	var $post_slider = $("#post-slider-775");
	var $post_slide_height = $post_slider.height();
	$post_slider.imagesLoaded(function(){
	$post_slider.css({'height': $post_slide_height + 'px'}).addClass("img_loaded");
	$($post_slider).lightSlider({
		prevHtml:'<i style="width:44px;height:44px;margin-top:-22px;font-size:44px;background:rgba(10,10,10,.3);" class="pe pe-7s-angle-left"></i>',
		nextHtml:'<i style="width:44px;height:44px;margin-top:-22px;font-size:44px;background:rgba(10,10,10,.3);" class="pe pe-7s-angle-right"></i>',
		item:1,
		slideMove:1,
		loop:true,
		slideMargin:0,		
		pauseOnHover:true,
		auto:true,
		pause:5000,
		speed:700,
		pager:false,		
		cssEasing: 'cubic-bezier(0.75, 0, 0.35, 1)',
		adaptiveHeight:true,
		keyPress:true,
		responsive:[{breakpoint:768,settings:{item:1}},{breakpoint:480,settings:{item:1}}]
		});
	});
});
</script><!-- START Post-intro --><div class="post_intro">
<div class="entry-header">
		
        					<h2 itemprop="name">
									<a href="/us/blog/case-studies/the-use-of-turbocad-in-conceptual-design-product-visualization" itemprop="url">
					The Use of TurboCAD in Conceptual Design &amp; Product Visualization</a>
							</h2>
								        	<dl class="article-info">

		
			<dt class="article-info-term"></dt>	
				
							<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
	<i class="fa fa-user"></i>
					<span itemprop="name" data-toggle="tooltip" title="Written by ">IMSI Design</span>	</dd>			
			
							<dd class="category-name">
	<i class="fa fa-folder-open-o"></i>
				<span itemprop="genre" itemprop="genre" data-toggle="tooltip" title="Article Category">Case Studies</span>	</dd>			
			
							<dd class="published">
	<i class="fa fa-calendar-o"></i>
	<time datetime="2017-05-18T16:33:09+00:00" itemprop="datePublished" data-toggle="tooltip" title="Published Date">
		18 May 2017	</time>
</dd>			
		
					
			
					
		
	</dl>
    </div>


		
<p>Netherlands-based Kras.Design specializes in designing aesthetic and mechanical solutions, and uses TurboCAD for conceptual design and product visualization. "Thanks to TurboCAD Pro Platinum, we have satisfied customers, and we can present our designs and promotional work much better," says owner Ralf Kras.</p>
    <div class="clearfix"></div>	
 
	
<div class="readmore">
	<a class="btn btn-readmore" href="/us/blog/case-studies/the-use-of-turbocad-in-conceptual-design-product-visualization" itemprop="url">
		Read More	</a>
</div>
	    <div class="helix-social-share">
        <div class="helix-social-share-blog">
            <ul>
            	                <li>
                    <div class="facebook" data-toggle="tooltip" data-placement="top" title="Share On Facebook">

                        <a class="facebook" onClick="window.open('http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/case-studies/the-use-of-turbocad-in-conceptual-design-product-visualization', 'Facebook', 'width=600,height=300,left=' + (screen.availWidth / 2 - 300) + ',top=' + (screen.availHeight / 2 - 150) + ''); return false;" href="http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/case-studies/the-use-of-turbocad-in-conceptual-design-product-visualization">
                            <i class="fa fa-facebook"></i>
                        </a>

                    </div>
                </li>
                				                <li>
                    <div class="twitter"  data-toggle="tooltip" data-placement="top" title="Share On Twitter">
                        <a class="twitter" onClick="window.open('http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/the-use-of-turbocad-in-conceptual-design-product-visualization&amp;text=The%20Use%20of%20TurboCAD%20in%20Conceptual%20Design%20&%20Product%20Visualization', 'Twitter share', 'width=600,height=300,left=' + (screen.availWidth / 2 - 300) + ',top=' + (screen.availHeight / 2 - 150) + ''); return false;" href="http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/the-use-of-turbocad-in-conceptual-design-product-visualization&amp;text=The Use of TurboCAD in Conceptual Design & Product Visualization">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </div>
                </li>
                                                <li>
                    <div class="google-plus" data-toggle="tooltip" data-placement="top" title="Share On Google Plus">
                        <a class="google-plus" onClick="window.open('https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/the-use-of-turbocad-in-conceptual-design-product-visualization', 'Google plus', 'width=585,height=666,left=' + (screen.availWidth / 2 - 292) + ',top=' + (screen.availHeight / 2 - 333) + ''); return false;" href="https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/the-use-of-turbocad-in-conceptual-design-product-visualization" >
                            <i class="fa fa-google-plus"></i></a>
                    </div>
                </li>
                                                <li>
                    <div class="linkedin" data-toggle="tooltip" data-placement="top" title="Share On Linkedin" >
                        <a class="linkedin" onClick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/case-studies/the-use-of-turbocad-in-conceptual-design-product-visualization', 'Linkedin', 'width=585,height=666,left=' + (screen.availWidth / 2 - 292) + ',top=' + (screen.availHeight / 2 - 333) + ''); return false;" href="http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/case-studies/the-use-of-turbocad-in-conceptual-design-product-visualization" >
                            <i class="fa fa-linkedin"></i></a>
                    </div>
                </li>
                            </ul>
        </div>
    </div> <!-- /.helix-social-share -->
    

	<div class="clearfix"></div>	
	          	<div class="clearfix"></div>
	    	    	<hr class="blog_hr" />
                </div><!-- END Post-intro -->
     

                        </article>
                        <!-- end item -->
                                            </div><!-- end col-sm-* -->
                                                                                                <div class="col-xs-12 col-sm-6 col-md-6 masonry_item">
                        <article class="item column-1"
                            itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting">
                            


<div class=" has-post-format-masonry">
	    <span class="post-format-masonry"><i style="margin-right:-6px;" class="fa fa-pencil-square-o"></i></span>
     
</div>




<div class="entry-image intro-image">
			<a href="/us/blog/case-studies/turbocad-helps-electronic-engineering-consultant-expand-into-mechanical-cad">
	
	<img
		src="images/Case-Studies/electronic-engineering-consultant-expands-into-mechanical-cad-turbocad-case-study.png" alt="" itemprop="thumbnailUrl"/>

			</a>
	</div>
<!-- START Post-intro --><div class="post_intro">
<div class="entry-header">
		
        					<h2 itemprop="name">
									<a href="/us/blog/case-studies/turbocad-helps-electronic-engineering-consultant-expand-into-mechanical-cad" itemprop="url">
					TurboCAD Helps Electronic Engineering Consultant Expand into Mechanical CAD</a>
							</h2>
								        	<dl class="article-info">

		
			<dt class="article-info-term"></dt>	
				
							<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
	<i class="fa fa-user"></i>
					<span itemprop="name" data-toggle="tooltip" title="Written by ">IMSI Design</span>	</dd>			
			
							<dd class="category-name">
	<i class="fa fa-folder-open-o"></i>
				<span itemprop="genre" itemprop="genre" data-toggle="tooltip" title="Article Category">Case Studies</span>	</dd>			
			
							<dd class="published">
	<i class="fa fa-calendar-o"></i>
	<time datetime="2017-05-18T20:11:07+00:00" itemprop="datePublished" data-toggle="tooltip" title="Published Date">
		18 May 2017	</time>
</dd>			
		
					
			
					
		
	</dl>
    </div>


		
<p>Jeff Lindstrom, CEO of Arvak Technologies, was originally an Electronic Engineering Consultant for small aerospace companies in Southern California, and founded Arvak Technologies in order to pursue business opportunities with the Defense Logistics Agency (DLA). After finding TurboCAD, Lindstrom was able to expand into mechanical CAD.</p>
    <div class="clearfix"></div>	
 
	
<div class="readmore">
	<a class="btn btn-readmore" href="/us/blog/case-studies/turbocad-helps-electronic-engineering-consultant-expand-into-mechanical-cad" itemprop="url">
		Read More	</a>
</div>
	    <div class="helix-social-share">
        <div class="helix-social-share-blog">
            <ul>
            	                <li>
                    <div class="facebook" data-toggle="tooltip" data-placement="top" title="Share On Facebook">

                        <a class="facebook" onClick="window.open('http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-helps-electronic-engineering-consultant-expand-into-mechanical-cad', 'Facebook', 'width=600,height=300,left=' + (screen.availWidth / 2 - 300) + ',top=' + (screen.availHeight / 2 - 150) + ''); return false;" href="http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-helps-electronic-engineering-consultant-expand-into-mechanical-cad">
                            <i class="fa fa-facebook"></i>
                        </a>

                    </div>
                </li>
                				                <li>
                    <div class="twitter"  data-toggle="tooltip" data-placement="top" title="Share On Twitter">
                        <a class="twitter" onClick="window.open('http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-helps-electronic-engineering-consultant-expand-into-mechanical-cad&amp;text=TurboCAD%20Helps%20Electronic%20Engineering%20Consultant%20Expand%20into%20Mechanical%20CAD', 'Twitter share', 'width=600,height=300,left=' + (screen.availWidth / 2 - 300) + ',top=' + (screen.availHeight / 2 - 150) + ''); return false;" href="http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-helps-electronic-engineering-consultant-expand-into-mechanical-cad&amp;text=TurboCAD Helps Electronic Engineering Consultant Expand into Mechanical CAD">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </div>
                </li>
                                                <li>
                    <div class="google-plus" data-toggle="tooltip" data-placement="top" title="Share On Google Plus">
                        <a class="google-plus" onClick="window.open('https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-helps-electronic-engineering-consultant-expand-into-mechanical-cad', 'Google plus', 'width=585,height=666,left=' + (screen.availWidth / 2 - 292) + ',top=' + (screen.availHeight / 2 - 333) + ''); return false;" href="https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-helps-electronic-engineering-consultant-expand-into-mechanical-cad" >
                            <i class="fa fa-google-plus"></i></a>
                    </div>
                </li>
                                                <li>
                    <div class="linkedin" data-toggle="tooltip" data-placement="top" title="Share On Linkedin" >
                        <a class="linkedin" onClick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-helps-electronic-engineering-consultant-expand-into-mechanical-cad', 'Linkedin', 'width=585,height=666,left=' + (screen.availWidth / 2 - 292) + ',top=' + (screen.availHeight / 2 - 333) + ''); return false;" href="http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-helps-electronic-engineering-consultant-expand-into-mechanical-cad" >
                            <i class="fa fa-linkedin"></i></a>
                    </div>
                </li>
                            </ul>
        </div>
    </div> <!-- /.helix-social-share -->
    

	<div class="clearfix"></div>	
	          	<div class="clearfix"></div>
	    	    	<hr class="blog_hr" />
                </div><!-- END Post-intro -->
     

                        </article>
                        <!-- end item -->
                                            </div><!-- end col-sm-* -->
                                            </div><!-- end row -->
                <div class="clearfix"></div>
           
    
	
		</div>
";s:4:"head";a:11:{s:5:"title";s:25:"IMSI Design - IMSI Design";s:11:"description";s:219:"IMSI Design is a leader in affordable, general-purpose CAD (Computer Aided Design) and home design desktop software, and a pioneer in mobile solutions for the AEC (Architectural, Engineering, and Construction) industry.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:4:"name";a:2:{s:8:"keywords";s:39:"IMSI Design, CAD, TurboCAD, home design";s:6:"rights";N;}}s:5:"links";a:2:{s:40:"/us/press-releases/blog/case-studies/rss";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:19:"application/rss+xml";s:5:"title";s:7:"RSS 2.0";}}s:41:"/us/press-releases/blog/case-studies/atom";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:20:"application/atom+xml";s:5:"title";s:8:"Atom 1.0";}}}s:11:"styleSheets";a:0:{}s:5:"style";a:1:{s:8:"text/css";s:234:".masonry_item .item .post_intro {padding:30px 0 0;}hr.blog_hr {margin-bottom:0}.masonry_item {margin:0 0 30px 0;}.masonry_item .item > div {margin-bottom:0;}.lSPrev,.lSNext{height:44px;width:44px;}.lSPrev>i,.lSNext>i{font-size:44px;}";}s:7:"scripts";a:4:{s:27:"/media/jui/js/jquery.min.js";a:2:{s:4:"type";s:15:"text/javascript";s:7:"options";a:6:{s:7:"version";s:4:"auto";s:8:"relative";b:1;s:11:"detectDebug";b:0;s:9:"framework";b:0;s:8:"pathOnly";b:0;s:13:"detectBrowser";b:1;}}s:34:"/media/jui/js/jquery-noconflict.js";a:2:{s:4:"type";s:15:"text/javascript";s:7:"options";a:6:{s:7:"version";s:4:"auto";s:8:"relative";b:1;s:9:"framework";b:0;s:8:"pathOnly";b:0;s:13:"detectBrowser";b:1;s:11:"detectDebug";b:1;}}s:35:"/media/jui/js/jquery-migrate.min.js";a:2:{s:4:"type";s:15:"text/javascript";s:7:"options";a:6:{s:7:"version";s:4:"auto";s:8:"relative";b:1;s:11:"detectDebug";b:0;s:9:"framework";b:0;s:8:"pathOnly";b:0;s:13:"detectBrowser";b:1;}}s:38:"/templates/flex/js/imagesloaded.min.js";a:2:{s:4:"type";s:15:"text/javascript";s:7:"options";a:0:{}}}s:6:"script";a:1:{s:15:"text/javascript";s:125:"
		jQuery(function($){jQuery(window).load(function() {jQuery(".items-masonry").masonry({itemSelector:".masonry_item"})})});
	";}s:6:"custom";a:0:{}s:10:"scriptText";a:0:{}}s:13:"mime_encoding";s:9:"text/html";s:7:"pathway";a:0:{}s:6:"module";a:0:{}}