<?php die("Access Denied"); ?>#x#a:5:{s:4:"body";s:13446:"<article class="item item-page" itemscope itemtype="http://schema.org/Article">
	<meta itemprop="inLanguage" content="en-US" />
		
		<div class="entry-image full-image"> <img
				src="images/blog/turbocad-pro-platinum-2017-50-new-improved-features.png" alt="" itemprop="image"/> </div>
        
	<div class="entry-header has-post-format">
		                            					            <span class="post-format"><i style="margin-right:-6px;" class="fa fa-pencil-square-o"></i></span>
                                        <h2 itemprop="name">
                                            IMSI Design Introduces TurboCAD Pro Platinum 2017                                    </h2>
             
            
            	<dl class="article-info">

		
			<dt class="article-info-term"></dt>	
				
							<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
	<i class="fa fa-user"></i>
					<span itemprop="name" data-toggle="tooltip" title="Written by ">IMSI Design</span>	</dd>			
			
							<dd class="category-name">
	<i class="fa fa-folder-open-o"></i>
				<span itemprop="genre" itemprop="genre" data-toggle="tooltip" title="Article Category">Press Releases</span>	</dd>			
			
							<dd class="published">
	<i class="fa fa-calendar-o"></i>
	<time datetime="2017-03-15T17:03:00+00:00" itemprop="datePublished" data-toggle="tooltip" title="Published Date">
		15 March 2017	</time>
</dd>			
		
					
			
					
		
	</dl>
  
        	</div>

	    
	
				
			<div itemprop="articleBody">
		
<p>NOVATO, Calif., March 15, 2017 - IMSI® Design, a leading CAD software developer, announced today the release of TurboCAD Pro Platinum 2017, the company’s premium 2D drafting and 3D Surface and Solid modeling application for Windows® desktop PCs.</p>
<p>This latest release of TurboCAD Pro Platinum boasts nearly 50 new and improved features. "We’ve continued to build on the functionality to both mechanical and architectural areas of design in the program, with special emphasis in this release to sheet metal design, CAD interoperability, and rendering," stated Bob Mayer, President of IMSI Design. A video showcasing the product’s new features can be viewed at: <a href="https://youtu.be/zW-Ra-FehPo">https://youtu.be/zW-Ra-FehPo</a></p>
<p>"TurboCAD continues to amaze me," writes Val Carter of Tri-CAD Technologies in a recent review of TurboCAD Pro Platinum 2017. As a dedicated user and developer of various TurboCAD training products since 2000, Carter adds, "What was most apparent this year is the amount of work the company has put into the creation of rock-solid operational stability as well as the introduction of project integration methodologies."</p>
<p>An overview of new and improved features include (please see www.TurboCAD.com for a complete listing):</p>
<h4>Usability &amp; Interface</h4>
<ul>
<li>New Intelligent File Send – E-pack, which gathers all design related content into a common folder with the drawing for easy distribution, now includes an option to send your packaged data via email directly from e-pack to speed productivity.</li>
<li>New Image Management Palette – Adds simple drag-and-drop insertion, control of image parameters, and embedding.</li>
<li>New Table Editing / Attachable Data – Build tables by making (non-OLE) connections to .CSV and Excel files. Simply edit table layout, format, and content, now similar to Microsoft Word.</li>
<li>New Timestamp – For security and collaboration, file metadata now includes Date of Creation, Date of Last Modification, Total File Editing Time, and Total Time of Last Session</li>
</ul>
<h4>2D Drafting &amp; Editing Features</h4>
<ul>
<li>New Center Line and Center Mark – Create associative center lines and center marks. Improved Multi-Text Editor – New text editor interface with new formatting features and new options including numbered lists, bulleted lists, and multi-column support.</li>
<li>New Scale by Two Points – Scale a set of objects to a specific size by using two reference points.</li>
<li>Improved Intelligent Scaling – Create hatches, text, and dimensions that automatically scale to the current zoom level of the view.</li>
</ul>
<h4>3D Drawing, Modeling &amp; Editing</h4>
<ul>
<li>New Bend by Sketch – Bend single or multiple flanges off of a sheet body using a 2D polyline as the profile.</li>
<li>New Gusset – Easily add support structures to 3D models. Use the New Sheet Metal Gusset to insert a sheet metal gusset between two adjacent faces.</li>
<li>New Solid Gusset can insert a Stiffening Rib between two adjacent faces.</li>
<li>Improved Law Tools Editor – Import and export Laws (Curve from Law; Surface with Laws; Offset with Law; Warp Entity by Law) from text files.</li>
<li>New Stretch Entity – Stretch any ACIS solid along an arbitrary axis.</li>
<li>New Twist Entity – Twist a solid object along and around the length of a designated axis</li>
<li>Improved NURBS Support – Guidelines for lofts can now be post-updated to modify the resulting loft. Now true NURB surfaces can be generated and edited in three axes.</li>
<li>New Unbend Options - New Get Bend Angles option to extract or insert bend angles as text when using the Unbend Sheet tool. New Unbend Along an Edge lets you selectively unbend elements of a sheet body along specified edges.</li>
<li>New &amp; Improved SMESH Tools (Sub-D Modeling) – New Combine and join adjacent coplanar faces; merge SMESHs; add or delete facets from a SMESH; create a volumetric object from a planar SMESH. Improvements to node selection which can be limited to ignore all but the closest nodes, and faces can now be split by a selected line.</li>
</ul>
<h4>Architectural Design Tools</h4>
<ul>
<li>New Copying of Architectural Objects – Use the copy and array tools to insert a set of windows or doors into a wall for greater productivity.</li>
<li>New Fit to Scale for Door and Window Custom Blocks – Automatic fit to wall width scaling of custom door and window blocks inserted into walls.</li>
<li>Improved House Builder Wizard – Create rooms on multiple workplanes for quick design of multi-story houses.</li>
<li>New Stair by Linework – Create complex and advanced stairways via a set of 2D linework.</li>
<li>New Roof Slab Openings – Insert openings into roof slab for fast placement of vents and skylights.</li>
</ul>
<h4>Photorealistic Rendering &amp; Visualization</h4>
<ul>
<li>New Redsdk 4.2 Engine Migration – Improved photorealistic rendering with new shaders, more realistic materials, new expanded parameters to control anti-ailiasing, caustics for sun lighting, physical sky lighting, geo-located sun positioning, and volumetric effects.</li>
<li>New UV Mapping of SMESH Objects – UV mapping has been expanded to support SMESH objects.</li>
</ul>
<h4>File Support / Interoperability</h4>
<ul>
<li>Improved AutoCAD (DXF, DWG, and DWF) File Compatibility – Implementation of the latest Teigha engine.</li>
<li>Improved 3D PDF (including .U3D and .PRC) Export – Support for export of multi-material objects.</li>
<li>Improved SketchUp (SKP) File Support – Import geometry, views, materials, and components from SketchUp files created in SketchUp Pro or SketchUp Mark versions 3.0 to 2017.</li>
<li>New Google Earth (KML and KMZ) File Support – Import KML and KMZ files into TurboCAD to view your designs in Google Earth independent of the web.</li>
<li>Improved Point Cloud (PCD, PCG, XYZ, ASC) File Support – Improved triangulation tool allows users to adjust the default parameter settings, offers an improved algorithm for facet normal determination, and a new smoothing option. Colored point clouds are also now supported.</li>
<li>New Mobile App Support - Import and View TurboApp (TAP) files from mobile devices.</li>
</ul>
<h3>Availability &amp; Pricing</h3>
<p>TurboCAD Pro Platinum 2017 is now available for $1,499.99 USD for a full, permanent license, or $499.99/year for an annual subscription.</p>
<p>For more information, download a 30-day trial, or to upgrade online, please visit <a href="http://www.TurboCAD.com">www.TurboCAD.com</a> or call IMSI Design at 1.800.833.8082 (+1.415.483.8000).</p>
<h3>About IMSI Design</h3>
<p>IMSI Design is a leader in affordable, general-purpose CAD (Computer Aided Design), and a pioneer in mobile solutions for the AEC (Architectural, Engineering, and Construction) industry. With over 16 million products distributed since 1988, IMSI Design products include the TurboCAD®, DesignCAD™, TurboFloorPlan®, TurboViewer®, TurboReview®, and TurboSite® families of precision design applications for desktop and mobile. Please visit <a href="http://www.IMSIdesign.com">www.IMSIdesign.com</a> for more information.</p>
<p><strong>Contact:</strong></p>
<p>IMSI Design<br /> Pam Volpe<br /> 415.483.8025<br /> <span id="cloak097c0c530aea34bc86702311d367d47d">JLIB_HTML_CLOAKING</span><script type='text/javascript'>
				document.getElementById('cloak097c0c530aea34bc86702311d367d47d').innerHTML = '';
				var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
				var path = 'hr' + 'ef' + '=';
				var addy097c0c530aea34bc86702311d367d47d = 'pr' + '&#64;';
				addy097c0c530aea34bc86702311d367d47d = addy097c0c530aea34bc86702311d367d47d + '&#105;ms&#105;d&#101;s&#105;gn' + '&#46;' + 'c&#111;m';
				var addy_text097c0c530aea34bc86702311d367d47d = 'pr' + '&#64;' + '&#105;ms&#105;d&#101;s&#105;gn' + '&#46;' + 'c&#111;m';document.getElementById('cloak097c0c530aea34bc86702311d367d47d').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy097c0c530aea34bc86702311d367d47d + '\'>'+addy_text097c0c530aea34bc86702311d367d47d+'<\/a>';
		</script></p>
<p>###</p>
<p>© 2017 IMSI Design, LLC. All rights reserved. IMSI and TurboCAD are registered trademarks and/or trademarks of IMSI Design, LLC. AutoCAD is a registered trademark of Autodesk. All other identifiable marks are the property of their respective owners.</p>	</div>

	    
    	<div class="helix-social-share">
		<div class="helix-social-share-blog helix-social-share-article">
			<ul>
								<li>
					<div class="facebook" data-toggle="tooltip" data-placement="top" title="Share On Facebook">

						<a class="facebook" onClick="window.open('http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-pro-platinum-2017','Facebook','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-pro-platinum-2017">
							<i class="fa fa-facebook-square"></i> Facebook						</a>

					</div>
				</li>
                								<li>
					<div class="twitter" data-toggle="tooltip" data-placement="top" title="Share On Twitter">
						<a class="twitter" onClick="window.open('http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-pro-platinum-2017&amp;text=IMSI Design Introduces TurboCAD Pro Platinum 2017','Twitter share','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-pro-platinum-2017&amp;text=IMSI%20Design%20Introduces%20TurboCAD%20Pro%20Platinum%202017">
							<i class="fa fa-twitter-square"></i> Twitter						</a>
					</div>
				</li>
                                				<li>
					<div class="google-plus">
						<a class="google-plus" data-toggle="tooltip" data-placement="top" title="Share On Google Plus" onClick="window.open('https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-pro-platinum-2017','Google plus','width=585,height=666,left='+(screen.availWidth/2-292)+',top='+(screen.availHeight/2-333)+''); return false;" href="https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-pro-platinum-2017" >
						<i class="fa fa-google-plus"></i></a>
					</div>
				</li>
				                				<li>
					<div class="linkedin">
						<a class="linkedin" data-toggle="tooltip" data-placement="top" title="Share On Linkedin" onClick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-pro-platinum-2017','Linkedin','width=585,height=666,left='+(screen.availWidth/2-292)+',top='+(screen.availHeight/2-333)+''); return false;" href="http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-pro-platinum-2017" >	
						<i class="fa fa-linkedin-square"></i></a>
					</div>
				</li>
                			</ul>
		</div>		
	</div> <!-- /.helix-social-share -->
    	<div style="margin:0 auto 35px;" class="clearfix"></div><hr />
        
        			
	
<nav role="pagination">
    <ul class="cd-pagination no-space animated-buttons custom-icons">
		            <li class="button btn-previous">
                <a href="/us/blog/press-releases/imsi-design-introduces-turbocad-expert-2017" rel="prev"><i>Prev</i></a>
            </li>
                
           </ul>
</nav>
				    
    
	</article>";s:4:"head";a:11:{s:5:"title";s:25:"IMSI Design - IMSI Design";s:11:"description";s:219:"IMSI Design is a leader in affordable, general-purpose CAD (Computer Aided Design) and home design desktop software, and a pioneer in mobile solutions for the AEC (Architectural, Engineering, and Construction) industry.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:4:"name";a:3:{s:8:"keywords";s:39:"IMSI Design, CAD, TurboCAD, home design";s:6:"rights";N;s:6:"author";s:11:"IMSI Design";}}s:5:"links";a:0:{}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:0:{}s:6:"script";a:0:{}s:6:"custom";a:7:{i:0;s:142:"<meta property="og:url" content="http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-pro-platinum-2017" />";i:1;s:45:"<meta property="og:type" content="article" />";i:2;s:88:"<meta property="og:title" content="IMSI Design Introduces TurboCAD Pro Platinum 2017" />";i:3;s:206:"<meta property="og:description" content="NOVATO, Calif., March 15, 2017 - IMSI® Design, a leading CAD software developer, announced today the release of TurboCAD Pro Platinum 2017, the company’s p..." />";i:4;s:139:"<meta property="og:image" content="http://test.imsitechnologies.com/images/blog/turbocad-pro-platinum-2017-50-new-improved-features.png" />";i:5;s:48:"<meta property="og:image:width" content="600" />";i:6;s:49:"<meta property="og:image:height" content="315" />";}s:10:"scriptText";a:0:{}}s:13:"mime_encoding";s:9:"text/html";s:7:"pathway";a:0:{}s:6:"module";a:0:{}}