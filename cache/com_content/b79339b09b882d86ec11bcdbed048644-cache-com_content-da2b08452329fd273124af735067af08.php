<?php die("Access Denied"); ?>#x#a:5:{s:4:"body";s:14840:"<article class="item item-page" itemscope itemtype="http://schema.org/Article">
	<meta itemprop="inLanguage" content="en-US" />
		
		<div class="entry-image full-image"> <img
				src="images/Case-Studies/custom-hardware-and-software-production-designcad-case-study.png" alt="" itemprop="image"/> </div>
        
	<div class="entry-header has-post-format">
		                            					            <span class="post-format"><i style="margin-right:-6px;" class="fa fa-pencil-square-o"></i></span>
                                        <h2 itemprop="name">
                                            Use of DesignCAD in Custom Hardware &amp; Software Production                                    </h2>
             
            
            	<dl class="article-info">

		
			<dt class="article-info-term"></dt>	
				
							<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
	<i class="fa fa-user"></i>
					<span itemprop="name" data-toggle="tooltip" title="Written by ">IMSI Design</span>	</dd>			
			
							<dd class="category-name">
	<i class="fa fa-folder-open-o"></i>
				<span itemprop="genre" itemprop="genre" data-toggle="tooltip" title="Article Category">Case Studies</span>	</dd>			
			
							<dd class="published">
	<i class="fa fa-calendar-o"></i>
	<time datetime="2017-05-19T16:13:58+00:00" itemprop="datePublished" data-toggle="tooltip" title="Published Date">
		19 May 2017	</time>
</dd>			
		
					
			
					
		
	</dl>
  
        	</div>

	    
	
				
			<div itemprop="articleBody">
		
<blockquote>DesignCAD is very easy to use and relatively easy to learn. It has the most customizable user interface of any program I have used, allowing us to tailor it to our personal way of working. – Phil Hays, Hardware Designer</blockquote>
<h4>Background:</h4>
<p>Alpha Omega Computer Systems, Inc. is a provider of custom hardware and software solutions for everything from oceanography, to satellite communications, to terrestrial RF networks, and anything in between and beyond. Since its establishment in 1977, the company has created products for basically every industrial and natural environment, and IMSI Design’s DesignCAD software has been used in more than 300 hardware and electronic design projects.</p>
<p>As a hardware designer with Alpha Omega, Phillip Hays has been creating electronic and mechanical designs for about 35 years, primarily electronic circuit design and circuit board layouts, but also mechanical enclosure design, for a very wide variety of customer projects.</p>
<h4>Challenges:</h4>
<p>Before finding DesignCAD (then called ProDesign) in the late 80’s, Alpha Omega was still using pencil and paper on drafting tables, which made for a very slow and tedious work process. "We were using Digital Equipment computers and considered a CAD package that ran on these computers," Phil Hays explains, "but it cost over $100,000, which was a bit too high for our budget." Instead, the company decided to try AutoCAD, which, although priced at over $1,000 at the time, was nothing more than a crude translation of pencil and paper design processes to a computer.</p>
<h4>Product Search:</h4>
<p>AutoCAD was so cumbersome to use that when Phil Hays and the Alpha Omega team began their search for a replacement CAD solution, their main criteria was to find a product that was not AutoCAD. They saw an ad for DesignCAD in an industry magazine, and decided to give it a try. Available alternatives at the time looked amateurish, incomplete, or were far too expensive. Unlike the other products they considered, DesignCAD, Hays explains, had a user interface that was far ahead of its time. "It was a work of genius: intuitive and allowed us to create drawings with about one fourth as many keystrokes – much easier than AutoCAD." Today, about 30 years later, Hays still considers DesignCAD a great CAD solution that he would happily recommend to other people: "Buy it! Learn it! Use it!" he says. "It is the easiest to use CAD program that I have seen, and it is very versatile."</p>
<blockquote>
<p>It was a work of genius: intuitive and allowed us to create drawings with about one fourth as many keystrokes – much easier than AutoCAD</p>
</blockquote>
<h4>Implementation &amp; Product Use:</h4>
<p>When Phil Hays and Alpha Omega Computer Systems first started using DesignCAD in 1987, the team first had to unlearn AutoCAD, in order to discover all the better ways of doing things that DesignCAD offered. Like any CAD software, DesignCAD also came with a learning curve; however, as far as CAD software goes, Phil Hays describes DesignCAD as very versatile and easy-to-use.</p>
<p>Alpha Omega is using DesignCAD in almost every way imaginable. As far as 2D design goes, the company does a lot of basic 2D drawings for electronic and mechanical designs. The software’s 3D design tools are used for designing enclosures and chassis. Additionally, DesignCAD is used to generate files for driving NC milling machines, and its CAD analysis features determine the volumes of molds for casting parts. "I also produce images from 3D designs for illustrations to show customers what a product will look like," Hays explains. "Occasionally we have used DesignCAD to produce 3D stereolith models. We even use DesignCAD to generate graphs for data analysis and product management charts."</p>
<blockquote>
<p>DesignCAD is the starting point for all of these designs. We use it to sketch the preliminary designs and then develop these into the working products.</p>
</blockquote>
<p>Overall, Alpha Omega has used DesignCAD in more than 300 hardware and electronic design projects, including oceanographic instruments, plywood lathe automation, walkthrough and handheld metal detectors, and many remote monitoring and data gathering instruments. "DesignCAD is the starting point for all of these designs," Phil Hays explains. "We use it to sketch the preliminary designs and then develop these into the working products."</p>
<p>As an example of a previous project, Alpha Omega Computer Systems was hired to create a walk-through metal detector, which was a quite complex project that involved working with several other companies in order to produce all the parts. Phil Hays says that Alpha Omega designed the electronics for the metal detector. “DesignCAD was used for the preliminary schematic designs because we could exchange files with other CAD programs used by the other companies, something that was not possible with the circuit board layout programs,” Hays explains. DesignCAD was then used to design the detector’s side panels, where the antennas were located. “I used DesignCAD to create several preliminary part layouts to determine how to fit all of the panels, cables, circuit boards, power supplies, batteries, etc., into the cabinet. Central to the unit was the electronics assembly that was housed in a sheet metal case. We wanted this box to be easily removable to allow for a quick exchange in the field.”</p>
<p>Using DesignCAD, Phil Hays designed the metal detector circuit boards for the general parts layouts and board sizes. Then he put together a CAD drawing of the stack of three circuit boards showing cable routing and the adjacent power supply and battery box. Finally, airflow requirements, fan placement, and exit holes were calculated to ensure air flow over the warmest components. The metal parts were also designed with DesignCAD, using two options for fabrication.</p>
<p>“I also used DesignCAD to determine parts positions, cable runs, and mounting arrangements inside the metal detector header assembly,” Phil Hays explains. “Images from DesignCAD were used in an operator’s manual and repair manual to illustrate how to install and remove the electronics package, and I generated an exploded parts diagram for the assembly instructions.”</p>
<p>At the moment, the company is using DesignCAD 3D Max to design a dental equipment sterilizer. In this project, the product is used for mechanical design of the enclosure/cabinet, as well as for preliminary electronic circuit design.</p>
<p>Despite their vast experience with DesignCAD, Alpha Omega’s DesignCAD users continue to learn and expand their use of DesignCAD. "Occasionally, we are still surprised to learn new tricks from each other," Phil Hays says, and adds that he is also always learning something new from the DesignCAD User Forum. As a dedicated member of the DesignCAD User Forum, Hays and other experienced users help new (and old) users learn new tricks and nuances of the software. "At any time of day there will be someone, somewhere, who can help you learn the program," he says, emphasizing the benefits of the User Forum when it comes to a new user’s initial and continued learning process.</p>
<h4>Results &amp; Benefits:</h4>
<p>To Alpha Omega Computer Systems, DesignCAD has been an invaluable tool for many years, and the product’s versatility has been proven time and time again. "We are very happy with DesignCAD, and how it has evolved to provide more features to help with solving our design challenges," Phil Hays says.</p>
<blockquote>We use DesignCAD for 2D and 3D mechanical design, for electronic schematic generation, graphing for data analysis, production flow charts, image generation for product illustration, and for advertising. The versatility of the program allows us to do all of these things with the same tool.</blockquote>
<p>The fact that Alpha Omega has been able to do so much with one and the same CAD solution has also been a huge benefit, as it has saved the company both time and money associated with learning and purchasing new programs.</p>
<p>"We use DesignCAD for 2D and 3D mechanical design, for electronic schematic generation, graphing for data analysis, production flow charts, image generation for product illustration, and for advertising," Phil Hays says. "The versatility of the program allows us to do all of these things with the same tool."</p>
<h4>Future Goals:</h4>
<p>Phil Hays and Alpha Omega Computer Systems are looking forward to continued DesignCAD use on both a professional and – for Hays – a personal level. One of his hobbies is 3D modeling, and he uses the software to generate illustrations for his ship modeling website, dedicated to a long-term project of creating a 1:96 scale model of the USS Oklahoma City, also called the Okie Boat. Several of Hays’ images have been used in textbooks and other publications. Another person at Alpha Omega also uses DesignCAD for hobby work, currently designing an experimental aircraft.</p>
<p>Finally, Phil Hays is looking forward to the continued growth of DesignCAD. He explains that what he likes the most is how the program is constantly evolving to make problem solving easier. "I have submitted requests for new features that would make my work easier, and many of these have been implemented," he says, and continues: "Right now the programmers are implementing new features that were requested by users. Many new features have been added in recent years to improve compatibility with other CAD software, improve 3D stereolith printer output, and to take advantage of the newest hardware features and operating systems. The program is getting better all the time."</p>	</div>

	    
    	<div class="helix-social-share">
		<div class="helix-social-share-blog helix-social-share-article">
			<ul>
								<li>
					<div class="facebook" data-toggle="tooltip" data-placement="top" title="Share On Facebook">

						<a class="facebook" onClick="window.open('http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/case-studies/use-of-designcad-in-custom-hardware-software-production','Facebook','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/case-studies/use-of-designcad-in-custom-hardware-software-production">
							<i class="fa fa-facebook-square"></i> Facebook						</a>

					</div>
				</li>
                								<li>
					<div class="twitter" data-toggle="tooltip" data-placement="top" title="Share On Twitter">
						<a class="twitter" onClick="window.open('http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/use-of-designcad-in-custom-hardware-software-production&amp;text=Use of DesignCAD in Custom Hardware & Software Production','Twitter share','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/use-of-designcad-in-custom-hardware-software-production&amp;text=Use%20of%20DesignCAD%20in%20Custom%20Hardware%20&%20Software%20Production">
							<i class="fa fa-twitter-square"></i> Twitter						</a>
					</div>
				</li>
                                				<li>
					<div class="google-plus">
						<a class="google-plus" data-toggle="tooltip" data-placement="top" title="Share On Google Plus" onClick="window.open('https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/use-of-designcad-in-custom-hardware-software-production','Google plus','width=585,height=666,left='+(screen.availWidth/2-292)+',top='+(screen.availHeight/2-333)+''); return false;" href="https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/use-of-designcad-in-custom-hardware-software-production" >
						<i class="fa fa-google-plus"></i></a>
					</div>
				</li>
				                				<li>
					<div class="linkedin">
						<a class="linkedin" data-toggle="tooltip" data-placement="top" title="Share On Linkedin" onClick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/case-studies/use-of-designcad-in-custom-hardware-software-production','Linkedin','width=585,height=666,left='+(screen.availWidth/2-292)+',top='+(screen.availHeight/2-333)+''); return false;" href="http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/case-studies/use-of-designcad-in-custom-hardware-software-production" >	
						<i class="fa fa-linkedin-square"></i></a>
					</div>
				</li>
                			</ul>
		</div>		
	</div> <!-- /.helix-social-share -->
    	<div style="margin:0 auto 35px;" class="clearfix"></div><hr />
        
        			
	
<nav role="pagination">
    <ul class="cd-pagination no-space animated-buttons custom-icons">
		        
                   <li class="button btn-next">
                <a href="/us/blog/case-studies/turbocad-increases-productivity-in-theater-set-design" rel="next"><i>Next</i></a>
            </li>
            </ul>
</nav>
				    
    
	</article>";s:4:"head";a:11:{s:5:"title";s:25:"IMSI Design - IMSI Design";s:11:"description";s:219:"IMSI Design is a leader in affordable, general-purpose CAD (Computer Aided Design) and home design desktop software, and a pioneer in mobile solutions for the AEC (Architectural, Engineering, and Construction) industry.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:4:"name";a:3:{s:8:"keywords";s:39:"IMSI Design, CAD, TurboCAD, home design";s:6:"rights";N;s:6:"author";s:11:"IMSI Design";}}s:5:"links";a:0:{}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:0:{}s:6:"script";a:0:{}s:6:"custom";a:7:{i:0;s:146:"<meta property="og:url" content="http://test.imsitechnologies.com/us/blog/case-studies/use-of-designcad-in-custom-hardware-software-production" />";i:1;s:45:"<meta property="og:type" content="article" />";i:2;s:96:"<meta property="og:title" content="Use of DesignCAD in Custom Hardware & Software Production" />";i:3;s:203:"<meta property="og:description" content="Alpha Omega Computer Systems, Inc. is a provider of custom hardware and software solutions for everything from oceanography, to satellite communications, t..." />";i:4;s:156:"<meta property="og:image" content="http://test.imsitechnologies.com/images/Case-Studies/custom-hardware-and-software-production-designcad-case-study.png" />";i:5;s:48:"<meta property="og:image:width" content="600" />";i:6;s:49:"<meta property="og:image:height" content="315" />";}s:10:"scriptText";a:0:{}}s:13:"mime_encoding";s:9:"text/html";s:7:"pathway";a:0:{}s:6:"module";a:0:{}}