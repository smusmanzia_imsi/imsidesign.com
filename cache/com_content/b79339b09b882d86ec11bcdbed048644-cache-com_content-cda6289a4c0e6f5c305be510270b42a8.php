<?php die("Access Denied"); ?>#x#a:5:{s:4:"body";s:9858:"<article class="item item-page" itemscope itemtype="http://schema.org/Article">
	<meta itemprop="inLanguage" content="en-US" />
		
		<div class="entry-image full-image"> <img
				src="images/Case-Studies/increased-productivity-in-theater-set-design-turbocad-case-study.png" alt="" itemprop="image"/> </div>
        
	<div class="entry-header has-post-format">
		                            					            <span class="post-format"><i style="margin-right:-6px;" class="fa fa-pencil-square-o"></i></span>
                                        <h2 itemprop="name">
                                            TurboCAD Increases Productivity in Theater Set Design                                    </h2>
             
            
            	<dl class="article-info">

		
			<dt class="article-info-term"></dt>	
				
							<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
	<i class="fa fa-user"></i>
					<span itemprop="name" data-toggle="tooltip" title="Written by ">IMSI Design</span>	</dd>			
			
							<dd class="category-name">
	<i class="fa fa-folder-open-o"></i>
				<span itemprop="genre" itemprop="genre" data-toggle="tooltip" title="Article Category">Case Studies</span>	</dd>			
			
							<dd class="published">
	<i class="fa fa-calendar-o"></i>
	<time datetime="2017-05-18T20:30:03+00:00" itemprop="datePublished" data-toggle="tooltip" title="Published Date">
		18 May 2017	</time>
</dd>			
		
					
			
					
		
	</dl>
  
        	</div>

	    
	
				
			<div itemprop="articleBody">
		
<blockquote>My archives show a count of 23,151 drawings I’ve created using TurboCAD. I could go on and on [about the software] but I won’t. I’m just a very satisfied customer who uses this program every workday. – Don Hook, Technical Director</blockquote>
<h4>Background:</h4>
<p>The Omaha Community Playhouse is a performing arts organization in Omaha, Nebraska. In addition to producing about 10 shows per season, the theater provides educational opportunities through the performing arts, offering a variety of classes, workshops, and volunteer opportunities for adults and children.</p>
<p>As the Technical Director of the Omaha Community Playhouse, Don Hook has been working with TurboCAD for many years, producing 2D drawings for set pieces. He is happy for an opportunity to brag about the product and its success in his organization.</p>
<h4>Product Use:</h4>
<p>Don Hook bought his first copy of TurboCAD at an Office Depot many years ago. Before then, everything was hand-drafted. "Vectorworks has a large presence in the arts, even though I personally have found it to be clunky and non-intuitive,” Hook explains, and mentions that TurboCAD’s ease-of-use, lower price tag, as well as its compatibility with other CAD solutions and file formats, are all reasons that he is a faithful, long-time user of the software. Designers he works with sometimes use AutoCAD or Vectorworks for scenic and lighting design, and TurboCAD has been practically seamless in the compatibility with these programs, using both DWG and DXF file systems.</p>
<p>At the time of this case study, the Omaha Community Playhouse produces 10 shows per season, all drawn in TurboCAD. Don Hook mostly creates shop drawings for the foreman and the carpenters to use in the construction of set pieces, so almost all work is done in 2D. "My 3D skills still need work, and more documentation to learn it," Hook says.</p>
<p>As the Technical Director, Don Hook is responsible for taking the designer’s concept and drawings and turning them into scenic elements used on stage. Hook explains that this includes everything from drawing furniture to walls to large pieces, such as turntables, flying devices, and automatic wagons that move scenery and actors on stage. "My archives show a count of 23,151 drawings I’ve created using TurboCAD," he says. "I could go on and on [about the software] but I won’t. I’m just a very satisfied customer who uses this program every workday."</p>
<blockquote>
<p>TurboCAD has increased my productivity immensely, and its ease-of-use has allowed changes on the fly when concepts or ideas change.</p>
</blockquote>
<p>In addition to day-to-day show production, the Omaha Community Playhouse offers an Apprenticeship Program, which brings students from local high schools and colleges to the theatre to learn the crafts and skills needed in technical theatre. It is a federally sanctioned program through the Bureau of Labor and has graduated over 100 students who have gone on to work for names like Disney and Cirque De Soleil, as well as several regional and touring theatre companies throughout the United States. Don Hook explains that the program is available through the local community college, the Metropolitan Community College, which handles the enrollment and classes taught at the theatre. "We are looking to set up a special topics class that one or two students can take each year that deals with learning drawing techniques specifically aimed at theatrical elements," he says.</p>
<p>Theater production often combines both architectural and mechanical drawing techniques, and TurboCAD has fit these needs well. As far as students go, Don Hook says most have had little or no experience even in mechanical drafting. However, "once they see how easy it is to learn the program, most can move quickly into the dos and don’ts of general drafting skills for the theatre, rather than have to struggle with learning both the CAD program and the drafting techniques at the same time."</p>
<h4>Results &amp; Benefits:</h4>
<p>As busy as the theater season is at the Omaha Community Playhouse, the use of TurboCAD has resulted in great time-savings for Don Hook and the theater. He explains: "TurboCAD has increased my productivity immensely, and its ease-of-use has allowed changes on the fly when concepts or ideas change." He also appreciates the new tools and features that come with very new version of TurboCAD. "Each update has brought more power and tools to the program until it’s matured into a program that rivals AutoCAD and Vectorworks."</p>	</div>

	    
    	<div class="helix-social-share">
		<div class="helix-social-share-blog helix-social-share-article">
			<ul>
								<li>
					<div class="facebook" data-toggle="tooltip" data-placement="top" title="Share On Facebook">

						<a class="facebook" onClick="window.open('http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-increases-productivity-in-theater-set-design','Facebook','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-increases-productivity-in-theater-set-design">
							<i class="fa fa-facebook-square"></i> Facebook						</a>

					</div>
				</li>
                								<li>
					<div class="twitter" data-toggle="tooltip" data-placement="top" title="Share On Twitter">
						<a class="twitter" onClick="window.open('http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-increases-productivity-in-theater-set-design&amp;text=TurboCAD Increases Productivity in Theater Set Design','Twitter share','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-increases-productivity-in-theater-set-design&amp;text=TurboCAD%20Increases%20Productivity%20in%20Theater%20Set%20Design">
							<i class="fa fa-twitter-square"></i> Twitter						</a>
					</div>
				</li>
                                				<li>
					<div class="google-plus">
						<a class="google-plus" data-toggle="tooltip" data-placement="top" title="Share On Google Plus" onClick="window.open('https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-increases-productivity-in-theater-set-design','Google plus','width=585,height=666,left='+(screen.availWidth/2-292)+',top='+(screen.availHeight/2-333)+''); return false;" href="https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-increases-productivity-in-theater-set-design" >
						<i class="fa fa-google-plus"></i></a>
					</div>
				</li>
				                				<li>
					<div class="linkedin">
						<a class="linkedin" data-toggle="tooltip" data-placement="top" title="Share On Linkedin" onClick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-increases-productivity-in-theater-set-design','Linkedin','width=585,height=666,left='+(screen.availWidth/2-292)+',top='+(screen.availHeight/2-333)+''); return false;" href="http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/case-studies/turbocad-increases-productivity-in-theater-set-design" >	
						<i class="fa fa-linkedin-square"></i></a>
					</div>
				</li>
                			</ul>
		</div>		
	</div> <!-- /.helix-social-share -->
    	<div style="margin:0 auto 35px;" class="clearfix"></div><hr />
        
        			
	
<nav role="pagination">
    <ul class="cd-pagination no-space animated-buttons custom-icons">
		            <li class="button btn-previous">
                <a href="/us/blog/case-studies/use-of-designcad-in-custom-hardware-software-production" rel="prev"><i>Prev</i></a>
            </li>
                
                   <li class="button btn-next">
                <a href="/us/blog/case-studies/turbocad-helps-electronic-engineering-consultant-expand-into-mechanical-cad" rel="next"><i>Next</i></a>
            </li>
            </ul>
</nav>
				    
    
	</article>";s:4:"head";a:11:{s:5:"title";s:25:"IMSI Design - IMSI Design";s:11:"description";s:219:"IMSI Design is a leader in affordable, general-purpose CAD (Computer Aided Design) and home design desktop software, and a pioneer in mobile solutions for the AEC (Architectural, Engineering, and Construction) industry.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:4:"name";a:3:{s:8:"keywords";s:39:"IMSI Design, CAD, TurboCAD, home design";s:6:"rights";N;s:6:"author";s:11:"IMSI Design";}}s:5:"links";a:0:{}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:0:{}s:6:"script";a:0:{}s:6:"custom";a:7:{i:0;s:144:"<meta property="og:url" content="http://test.imsitechnologies.com/us/blog/case-studies/turbocad-increases-productivity-in-theater-set-design" />";i:1;s:45:"<meta property="og:type" content="article" />";i:2;s:92:"<meta property="og:title" content="TurboCAD Increases Productivity in Theater Set Design" />";i:3;s:203:"<meta property="og:description" content="The Omaha Community Playhouse is a performing arts organization in Omaha, Nebraska. Technical Director Don Hook has used TurboCAD for many years, to produc..." />";i:4;s:160:"<meta property="og:image" content="http://test.imsitechnologies.com/images/Case-Studies/increased-productivity-in-theater-set-design-turbocad-case-study.png" />";i:5;s:48:"<meta property="og:image:width" content="600" />";i:6;s:49:"<meta property="og:image:height" content="315" />";}s:10:"scriptText";a:0:{}}s:13:"mime_encoding";s:9:"text/html";s:7:"pathway";a:0:{}s:6:"module";a:0:{}}