<?php die("Access Denied"); ?>#x#a:5:{s:4:"body";s:18290:"<div class="blog" itemscope itemtype="http://schema.org/Blog">
	
	
	
	
	
		
		    
                                    <div class="items-row items-masonry row clearfix">
                                                                                <div class="col-xs-12 col-sm-6 col-md-4 masonry_item">
                        <article class="item column-1"
                            itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting">
                            


<div class=" has-post-format-masonry">
	    <span class="post-format-masonry"><i style="margin-right:-6px;" class="fa fa-pencil-square-o"></i></span>
     
</div>




<div class="entry-image intro-image">
			<a href="/us/blog/press-releases/imsi-design-releases-turbocad-deluxe-2017">
	
	<img
		src="images/blog/turbocad-deluxe-2d-3d-2017-over-20-new-improved-features.png" alt="" itemprop="thumbnailUrl"/>

			</a>
	</div>
<!-- START Post-intro --><div class="post_intro">
<div class="entry-header">
		
        					<h2 itemprop="name">
									<a href="/us/blog/press-releases/imsi-design-releases-turbocad-deluxe-2017" itemprop="url">
					IMSI Design Releases TurboCAD Deluxe 2017</a>
							</h2>
								        	<dl class="article-info">

		
			<dt class="article-info-term"></dt>	
				
							<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
	<i class="fa fa-user"></i>
					<span itemprop="name" data-toggle="tooltip" title="Written by ">IMSI Design</span>	</dd>			
			
							<dd class="category-name">
	<i class="fa fa-folder-open-o"></i>
				<span itemprop="genre" itemprop="genre" data-toggle="tooltip" title="Article Category">Press Releases</span>	</dd>			
			
							<dd class="published">
	<i class="fa fa-calendar-o"></i>
	<time datetime="2017-05-05T17:03:00+00:00" itemprop="datePublished" data-toggle="tooltip" title="Published Date">
		05 May 2017	</time>
</dd>			
		
					
			
					
		
	</dl>
    </div>


		
<p>NOVATO, Calif., May 5, 2017 – IMSI® Design, a leading CAD software developer, announced today the release of TurboCAD Deluxe 2017, a complete 2D drafting and 3D modeling application for Windows® desktop PCs.</p>
    <div class="clearfix"></div>	
 
	
<div class="readmore">
	<a class="btn btn-readmore" href="/us/blog/press-releases/imsi-design-releases-turbocad-deluxe-2017" itemprop="url">
		Read More	</a>
</div>
	    <div class="helix-social-share">
        <div class="helix-social-share-blog">
            <ul>
            	                <li>
                    <div class="facebook" data-toggle="tooltip" data-placement="top" title="Share On Facebook">

                        <a class="facebook" onClick="window.open('http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-releases-turbocad-deluxe-2017', 'Facebook', 'width=600,height=300,left=' + (screen.availWidth / 2 - 300) + ',top=' + (screen.availHeight / 2 - 150) + ''); return false;" href="http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-releases-turbocad-deluxe-2017">
                            <i class="fa fa-facebook"></i>
                        </a>

                    </div>
                </li>
                				                <li>
                    <div class="twitter"  data-toggle="tooltip" data-placement="top" title="Share On Twitter">
                        <a class="twitter" onClick="window.open('http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-releases-turbocad-deluxe-2017&amp;text=IMSI%20Design%20Releases%20TurboCAD%20Deluxe%202017', 'Twitter share', 'width=600,height=300,left=' + (screen.availWidth / 2 - 300) + ',top=' + (screen.availHeight / 2 - 150) + ''); return false;" href="http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-releases-turbocad-deluxe-2017&amp;text=IMSI Design Releases TurboCAD Deluxe 2017">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </div>
                </li>
                                                <li>
                    <div class="google-plus" data-toggle="tooltip" data-placement="top" title="Share On Google Plus">
                        <a class="google-plus" onClick="window.open('https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-releases-turbocad-deluxe-2017', 'Google plus', 'width=585,height=666,left=' + (screen.availWidth / 2 - 292) + ',top=' + (screen.availHeight / 2 - 333) + ''); return false;" href="https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-releases-turbocad-deluxe-2017" >
                            <i class="fa fa-google-plus"></i></a>
                    </div>
                </li>
                                                <li>
                    <div class="linkedin" data-toggle="tooltip" data-placement="top" title="Share On Linkedin" >
                        <a class="linkedin" onClick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-releases-turbocad-deluxe-2017', 'Linkedin', 'width=585,height=666,left=' + (screen.availWidth / 2 - 292) + ',top=' + (screen.availHeight / 2 - 333) + ''); return false;" href="http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-releases-turbocad-deluxe-2017" >
                            <i class="fa fa-linkedin"></i></a>
                    </div>
                </li>
                            </ul>
        </div>
    </div> <!-- /.helix-social-share -->
    

	<div class="clearfix"></div>	
	          	<div class="clearfix"></div>
	    	    	<hr class="blog_hr" />
                </div><!-- END Post-intro -->
     

                        </article>
                        <!-- end item -->
                                            </div><!-- end col-sm-* -->
                                                                                                <div class="col-xs-12 col-sm-6 col-md-4 masonry_item">
                        <article class="item column-2"
                            itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting">
                            


<div class=" has-post-format-masonry">
	    <span class="post-format-masonry"><i style="margin-right:-6px;" class="fa fa-pencil-square-o"></i></span>
     
</div>




<div class="entry-image intro-image">
			<a href="/us/blog/press-releases/imsi-design-introduces-turbocad-expert-2017">
	
	<img
		src="images/blog/new-turbocad-expert-2017-cad-for-advanced-users.png" alt="" itemprop="thumbnailUrl"/>

			</a>
	</div>
<!-- START Post-intro --><div class="post_intro">
<div class="entry-header">
		
        					<h2 itemprop="name">
									<a href="/us/blog/press-releases/imsi-design-introduces-turbocad-expert-2017" itemprop="url">
					IMSI Design Introduces TurboCAD Expert 2017</a>
							</h2>
								        	<dl class="article-info">

		
			<dt class="article-info-term"></dt>	
				
							<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
	<i class="fa fa-user"></i>
					<span itemprop="name" data-toggle="tooltip" title="Written by ">IMSI Design</span>	</dd>			
			
							<dd class="category-name">
	<i class="fa fa-folder-open-o"></i>
				<span itemprop="genre" itemprop="genre" data-toggle="tooltip" title="Article Category">Press Releases</span>	</dd>			
			
							<dd class="published">
	<i class="fa fa-calendar-o"></i>
	<time datetime="2017-04-12T17:03:00+00:00" itemprop="datePublished" data-toggle="tooltip" title="Published Date">
		12 April 2017	</time>
</dd>			
		
					
			
					
		
	</dl>
    </div>


		
<p>NOVATO, Calif., April 12, 2017 – IMSI® Design, a leading CAD software developer, today announced a new addition to its TurboCAD product line titled TurboCAD Expert 2017, a 2D/3D CAD application for Windows® desktop PCs.</p>
    <div class="clearfix"></div>	
 
	
<div class="readmore">
	<a class="btn btn-readmore" href="/us/blog/press-releases/imsi-design-introduces-turbocad-expert-2017" itemprop="url">
		Read More	</a>
</div>
	    <div class="helix-social-share">
        <div class="helix-social-share-blog">
            <ul>
            	                <li>
                    <div class="facebook" data-toggle="tooltip" data-placement="top" title="Share On Facebook">

                        <a class="facebook" onClick="window.open('http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-expert-2017', 'Facebook', 'width=600,height=300,left=' + (screen.availWidth / 2 - 300) + ',top=' + (screen.availHeight / 2 - 150) + ''); return false;" href="http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-expert-2017">
                            <i class="fa fa-facebook"></i>
                        </a>

                    </div>
                </li>
                				                <li>
                    <div class="twitter"  data-toggle="tooltip" data-placement="top" title="Share On Twitter">
                        <a class="twitter" onClick="window.open('http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-expert-2017&amp;text=IMSI%20Design%20Introduces%20TurboCAD%20Expert%202017', 'Twitter share', 'width=600,height=300,left=' + (screen.availWidth / 2 - 300) + ',top=' + (screen.availHeight / 2 - 150) + ''); return false;" href="http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-expert-2017&amp;text=IMSI Design Introduces TurboCAD Expert 2017">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </div>
                </li>
                                                <li>
                    <div class="google-plus" data-toggle="tooltip" data-placement="top" title="Share On Google Plus">
                        <a class="google-plus" onClick="window.open('https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-expert-2017', 'Google plus', 'width=585,height=666,left=' + (screen.availWidth / 2 - 292) + ',top=' + (screen.availHeight / 2 - 333) + ''); return false;" href="https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-expert-2017" >
                            <i class="fa fa-google-plus"></i></a>
                    </div>
                </li>
                                                <li>
                    <div class="linkedin" data-toggle="tooltip" data-placement="top" title="Share On Linkedin" >
                        <a class="linkedin" onClick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-expert-2017', 'Linkedin', 'width=585,height=666,left=' + (screen.availWidth / 2 - 292) + ',top=' + (screen.availHeight / 2 - 333) + ''); return false;" href="http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-expert-2017" >
                            <i class="fa fa-linkedin"></i></a>
                    </div>
                </li>
                            </ul>
        </div>
    </div> <!-- /.helix-social-share -->
    

	<div class="clearfix"></div>	
	          	<div class="clearfix"></div>
	    	    	<hr class="blog_hr" />
                </div><!-- END Post-intro -->
     

                        </article>
                        <!-- end item -->
                                            </div><!-- end col-sm-* -->
                                                                                                <div class="col-xs-12 col-sm-6 col-md-4 masonry_item">
                        <article class="item column-3"
                            itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting">
                            


<div class=" has-post-format-masonry">
	    <span class="post-format-masonry"><i style="margin-right:-6px;" class="fa fa-pencil-square-o"></i></span>
     
</div>




<div class="entry-image intro-image">
			<a href="/us/blog/press-releases/imsi-design-introduces-turbocad-pro-platinum-2017">
	
	<img
		src="images/blog/turbocad-pro-platinum-2017-50-new-improved-features.png" alt="" itemprop="thumbnailUrl"/>

			</a>
	</div>
<!-- START Post-intro --><div class="post_intro">
<div class="entry-header">
		
        					<h2 itemprop="name">
									<a href="/us/blog/press-releases/imsi-design-introduces-turbocad-pro-platinum-2017" itemprop="url">
					IMSI Design Introduces TurboCAD Pro Platinum 2017</a>
							</h2>
								        	<dl class="article-info">

		
			<dt class="article-info-term"></dt>	
				
							<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
	<i class="fa fa-user"></i>
					<span itemprop="name" data-toggle="tooltip" title="Written by ">IMSI Design</span>	</dd>			
			
							<dd class="category-name">
	<i class="fa fa-folder-open-o"></i>
				<span itemprop="genre" itemprop="genre" data-toggle="tooltip" title="Article Category">Press Releases</span>	</dd>			
			
							<dd class="published">
	<i class="fa fa-calendar-o"></i>
	<time datetime="2017-03-15T17:03:00+00:00" itemprop="datePublished" data-toggle="tooltip" title="Published Date">
		15 March 2017	</time>
</dd>			
		
					
			
					
		
	</dl>
    </div>


		
<p>NOVATO, Calif., March 15, 2017 - IMSI® Design, a leading CAD software developer, announced today the release of TurboCAD Pro Platinum 2017, the company’s premium 2D drafting and 3D Surface and Solid modeling application for Windows® desktop PCs.</p>
    <div class="clearfix"></div>	
 
	
<div class="readmore">
	<a class="btn btn-readmore" href="/us/blog/press-releases/imsi-design-introduces-turbocad-pro-platinum-2017" itemprop="url">
		Read More	</a>
</div>
	    <div class="helix-social-share">
        <div class="helix-social-share-blog">
            <ul>
            	                <li>
                    <div class="facebook" data-toggle="tooltip" data-placement="top" title="Share On Facebook">

                        <a class="facebook" onClick="window.open('http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-pro-platinum-2017', 'Facebook', 'width=600,height=300,left=' + (screen.availWidth / 2 - 300) + ',top=' + (screen.availHeight / 2 - 150) + ''); return false;" href="http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-pro-platinum-2017">
                            <i class="fa fa-facebook"></i>
                        </a>

                    </div>
                </li>
                				                <li>
                    <div class="twitter"  data-toggle="tooltip" data-placement="top" title="Share On Twitter">
                        <a class="twitter" onClick="window.open('http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-pro-platinum-2017&amp;text=IMSI%20Design%20Introduces%20TurboCAD%20Pro%20Platinum%202017', 'Twitter share', 'width=600,height=300,left=' + (screen.availWidth / 2 - 300) + ',top=' + (screen.availHeight / 2 - 150) + ''); return false;" href="http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-pro-platinum-2017&amp;text=IMSI Design Introduces TurboCAD Pro Platinum 2017">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </div>
                </li>
                                                <li>
                    <div class="google-plus" data-toggle="tooltip" data-placement="top" title="Share On Google Plus">
                        <a class="google-plus" onClick="window.open('https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-pro-platinum-2017', 'Google plus', 'width=585,height=666,left=' + (screen.availWidth / 2 - 292) + ',top=' + (screen.availHeight / 2 - 333) + ''); return false;" href="https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-pro-platinum-2017" >
                            <i class="fa fa-google-plus"></i></a>
                    </div>
                </li>
                                                <li>
                    <div class="linkedin" data-toggle="tooltip" data-placement="top" title="Share On Linkedin" >
                        <a class="linkedin" onClick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-pro-platinum-2017', 'Linkedin', 'width=585,height=666,left=' + (screen.availWidth / 2 - 292) + ',top=' + (screen.availHeight / 2 - 333) + ''); return false;" href="http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-introduces-turbocad-pro-platinum-2017" >
                            <i class="fa fa-linkedin"></i></a>
                    </div>
                </li>
                            </ul>
        </div>
    </div> <!-- /.helix-social-share -->
    

	<div class="clearfix"></div>	
	          	<div class="clearfix"></div>
	    	    	<hr class="blog_hr" />
                </div><!-- END Post-intro -->
     

                        </article>
                        <!-- end item -->
                                            </div><!-- end col-sm-* -->
                                            </div><!-- end row -->
                <div class="clearfix"></div>
           
    
	
		</div>
";s:4:"head";a:11:{s:5:"title";s:42:"Press Releases | IMSI Design - IMSI Design";s:11:"description";s:219:"IMSI Design is a leader in affordable, general-purpose CAD (Computer Aided Design) and home design desktop software, and a pioneer in mobile solutions for the AEC (Architectural, Engineering, and Construction) industry.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:4:"name";a:2:{s:8:"keywords";s:39:"IMSI Design, CAD, TurboCAD, home design";s:6:"rights";N;}}s:5:"links";a:2:{s:42:"/us/press-releases/blog/press-releases/rss";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:19:"application/rss+xml";s:5:"title";s:7:"RSS 2.0";}}s:43:"/us/press-releases/blog/press-releases/atom";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:20:"application/atom+xml";s:5:"title";s:8:"Atom 1.0";}}}s:11:"styleSheets";a:0:{}s:5:"style";a:1:{s:8:"text/css";s:157:".masonry_item .item .post_intro {padding:30px 0 0;}hr.blog_hr {margin-bottom:0}.masonry_item {margin:0 0 30px 0;}.masonry_item .item > div {margin-bottom:0;}";}s:7:"scripts";a:0:{}s:6:"script";a:1:{s:15:"text/javascript";s:125:"
		jQuery(function($){jQuery(window).load(function() {jQuery(".items-masonry").masonry({itemSelector:".masonry_item"})})});
	";}s:6:"custom";a:0:{}s:10:"scriptText";a:0:{}}s:13:"mime_encoding";s:9:"text/html";s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:7:"Company";s:4:"link";s:20:"index.php?Itemid=427";}i:1;O:8:"stdClass":2:{s:4:"name";s:14:"Press Releases";s:4:"link";s:71:"index.php?option=com_content&view=category&layout=blog&id=27&Itemid=429";}}s:6:"module";a:0:{}}