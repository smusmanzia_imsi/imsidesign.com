<?php die("Access Denied"); ?>#x#a:5:{s:4:"body";s:7797:"<div class="blog" itemscope itemtype="http://schema.org/Blog">
	
	
	
	
	
				<div class="items-leading clearfix">
							<article class="item leading-0"
					itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting">
					


<div class=" has-post-format-masonry">
	    <span class="post-format-masonry"><i style="margin-right:-6px;" class="fa fa-pencil-square-o"></i></span>
     
</div>




<div class="entry-image intro-image">
			<a href="/us/blog/cad-workflow-integration-between-the-office-and-the-field">
	
	<img
		src="images/blog/cad-workflow-turbocad-turbosite.png" alt="" itemprop="thumbnailUrl"/>

			</a>
	</div>
<!-- START Post-intro --><div class="post_intro">
<div class="entry-header">
		
        					<h2 itemprop="name">
									<a href="/us/blog/cad-workflow-integration-between-the-office-and-the-field" itemprop="url">
					CAD Workflow Integration Between the Office and the Field</a>
							</h2>
								        	<dl class="article-info">

		
			<dt class="article-info-term"></dt>	
				
							<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
	<i class="fa fa-user"></i>
					<span itemprop="name" data-toggle="tooltip" title="Written by ">IMSI Design</span>	</dd>			
			
							<dd class="category-name">
	<i class="fa fa-folder-open-o"></i>
				<span itemprop="genre" itemprop="genre" data-toggle="tooltip" title="Article Category">Blog</span>	</dd>			
			
							<dd class="published">
	<i class="fa fa-calendar-o"></i>
	<time datetime="2017-05-24T21:16:33+00:00" itemprop="datePublished" data-toggle="tooltip" title="Published Date">
		24 May 2017	</time>
</dd>			
		
					
			
					
		
	</dl>
    </div>


		
<p>With the release of TurboCAD 2017, users will find a more seamless workflow between office design and editing, and field work documentation. Office staff can now take TurboCAD drawing sand upload them to an iOS mobile device, where the files can then be used with IMSI Design’s field documentation app, TurboSite, in order to capture project and site information. Perfect for building construction, site design, job site inspections, construction administration, as-built verification, or practically any other AEC-related fieldwork project.</p>
<p>The captured information can then be round-tripped back into TurboCAD 2017 through the program’s new ability to read TurboSite’s native TAP files. Export the TAP file back to the office via email or upload to DropBox, Box or any WebDAV-enabled cloud site.</p>
<p>Back in the office, you can open up the TAP file in the new GeoMarks Palette. The Tap file will contain all mark-ups and GeoMarks, which include all captured media such as photos, videos and notes taken onsite.</p>
<p>Noted changes from the field visit that need to be made to the original file can then easily be made in TurboCAD.</p>
<p>To see a real-world example of workflow integration with TurboCAD and TurboSite, watch this video:</p>
<p><iframe src="https://www.youtube.com/embed/QrCQpTR2wmY" width="800" height="450" frameborder="0" allowfullscreen=""></iframe></p>
<p>Working with TurboCAD and TurboSite together allows you to drive project efficiency across more of the project lifecycle, streamlining workflow processes and increasing productivity.</p>
	<div class="clearfix"></div>	
		
        	<div class="tags">
	    <span><i class="fa fa-tags hasTooltip" title="Tags"></i></span>
																	<a href="/us/tags/turbosite" class="" rel="tag">TurboSite</a>
				,	
																								<a href="/us/tags/turbocad" class="" rel="tag">TurboCAD</a>
				,	
																								<a href="/us/tags/cad-workflow" class="" rel="tag">CAD Workflow</a>
					
										</div>
        	       <div class="helix-social-share">
        <div class="helix-social-share-blog">
            <ul>
            	                <li>
                    <div class="facebook" data-toggle="tooltip" data-placement="top" title="Share On Facebook">

                        <a class="facebook" onClick="window.open('http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/cad-workflow-integration-between-the-office-and-the-field', 'Facebook', 'width=600,height=300,left=' + (screen.availWidth / 2 - 300) + ',top=' + (screen.availHeight / 2 - 150) + ''); return false;" href="http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/cad-workflow-integration-between-the-office-and-the-field">
                            <i class="fa fa-facebook"></i>
                        </a>

                    </div>
                </li>
                				                <li>
                    <div class="twitter"  data-toggle="tooltip" data-placement="top" title="Share On Twitter">
                        <a class="twitter" onClick="window.open('http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/cad-workflow-integration-between-the-office-and-the-field&amp;text=CAD%20Workflow%20Integration%20Between%20the%20Office%20and%20the%20Field', 'Twitter share', 'width=600,height=300,left=' + (screen.availWidth / 2 - 300) + ',top=' + (screen.availHeight / 2 - 150) + ''); return false;" href="http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/cad-workflow-integration-between-the-office-and-the-field&amp;text=CAD Workflow Integration Between the Office and the Field">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </div>
                </li>
                                                <li>
                    <div class="google-plus" data-toggle="tooltip" data-placement="top" title="Share On Google Plus">
                        <a class="google-plus" onClick="window.open('https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/cad-workflow-integration-between-the-office-and-the-field', 'Google plus', 'width=585,height=666,left=' + (screen.availWidth / 2 - 292) + ',top=' + (screen.availHeight / 2 - 333) + ''); return false;" href="https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/cad-workflow-integration-between-the-office-and-the-field" >
                            <i class="fa fa-google-plus"></i></a>
                    </div>
                </li>
                                                <li>
                    <div class="linkedin" data-toggle="tooltip" data-placement="top" title="Share On Linkedin" >
                        <a class="linkedin" onClick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/cad-workflow-integration-between-the-office-and-the-field', 'Linkedin', 'width=585,height=666,left=' + (screen.availWidth / 2 - 292) + ',top=' + (screen.availHeight / 2 - 333) + ''); return false;" href="http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/cad-workflow-integration-between-the-office-and-the-field" >
                            <i class="fa fa-linkedin"></i></a>
                    </div>
                </li>
                            </ul>
        </div>
    </div> <!-- /.helix-social-share -->
	  	<div class="clearfix"></div>
	    	    	<hr class="blog_hr" />
                </div><!-- END Post-intro -->
     

				</article><hr />
									</div><!-- end items-leading -->
	
		    
           
    
	
			<div class="cat-children">
							<h3> Subcategories </h3>
						
					<div class="first">
									<h3 class="page-header item-title"><a href="/us/blog/case-studies">
				Case Studies</a>
				
							</h3>
			
									
					</div>
							<div class="last">
									<h3 class="page-header item-title"><a href="/us/blog/press-releases">
				Press Releases</a>
				
							</h3>
			
									
					</div>
			
 </div>
		</div>
";s:4:"head";a:11:{s:5:"title";s:25:"IMSI Design - IMSI Design";s:11:"description";s:219:"IMSI Design is a leader in affordable, general-purpose CAD (Computer Aided Design) and home design desktop software, and a pioneer in mobile solutions for the AEC (Architectural, Engineering, and Construction) industry.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:4:"name";a:2:{s:8:"keywords";s:39:"IMSI Design, CAD, TurboCAD, home design";s:6:"rights";N;}}s:5:"links";a:2:{s:27:"/us/press-releases/blog/rss";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:19:"application/rss+xml";s:5:"title";s:7:"RSS 2.0";}}s:28:"/us/press-releases/blog/atom";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:20:"application/atom+xml";s:5:"title";s:8:"Atom 1.0";}}}s:11:"styleSheets";a:0:{}s:5:"style";a:1:{s:8:"text/css";s:157:".masonry_item .item .post_intro {padding:30px 0 0;}hr.blog_hr {margin-bottom:0}.masonry_item {margin:0 0 30px 0;}.masonry_item .item > div {margin-bottom:0;}";}s:7:"scripts";a:4:{s:27:"/media/jui/js/jquery.min.js";a:2:{s:4:"type";s:15:"text/javascript";s:7:"options";a:6:{s:7:"version";s:4:"auto";s:8:"relative";b:1;s:11:"detectDebug";b:0;s:9:"framework";b:0;s:8:"pathOnly";b:0;s:13:"detectBrowser";b:1;}}s:34:"/media/jui/js/jquery-noconflict.js";a:2:{s:4:"type";s:15:"text/javascript";s:7:"options";a:6:{s:7:"version";s:4:"auto";s:8:"relative";b:1;s:9:"framework";b:0;s:8:"pathOnly";b:0;s:13:"detectBrowser";b:1;s:11:"detectDebug";b:1;}}s:35:"/media/jui/js/jquery-migrate.min.js";a:2:{s:4:"type";s:15:"text/javascript";s:7:"options";a:6:{s:7:"version";s:4:"auto";s:8:"relative";b:1;s:11:"detectDebug";b:0;s:9:"framework";b:0;s:8:"pathOnly";b:0;s:13:"detectBrowser";b:1;}}s:30:"/media/jui/js/bootstrap.min.js";a:2:{s:4:"type";s:15:"text/javascript";s:7:"options";a:6:{s:7:"version";s:4:"auto";s:8:"relative";b:1;s:11:"detectDebug";s:1:"0";s:9:"framework";b:0;s:8:"pathOnly";b:0;s:13:"detectBrowser";b:1;}}}s:6:"script";a:1:{s:15:"text/javascript";s:211:"jQuery(function($){ $(".hasTooltip").tooltip({"html": true,"container": "body"}); });
		jQuery(function($){jQuery(window).load(function() {jQuery(".items-masonry").masonry({itemSelector:".masonry_item"})})});
	";}s:6:"custom";a:0:{}s:10:"scriptText";a:0:{}}s:13:"mime_encoding";s:9:"text/html";s:7:"pathway";a:0:{}s:6:"module";a:0:{}}