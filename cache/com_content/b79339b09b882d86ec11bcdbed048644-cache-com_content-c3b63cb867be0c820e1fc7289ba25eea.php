<?php die("Access Denied"); ?>#x#a:5:{s:4:"body";s:6648:"<article class="item item-page" itemscope itemtype="http://schema.org/Article">
	<meta itemprop="inLanguage" content="en-US" />
		
		<div class="entry-image full-image"> <img
				src="images/blog/cad-workflow-turbocad-turbosite.png" alt="" itemprop="image"/> </div>
        
	<div class="entry-header has-post-format">
		                            					            <span class="post-format"><i style="margin-right:-6px;" class="fa fa-pencil-square-o"></i></span>
                                        <h2 itemprop="name">
                                            CAD Workflow Integration Between the Office and the Field                                    </h2>
             
            
            	<dl class="article-info">

		
			<dt class="article-info-term"></dt>	
				
							<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
	<i class="fa fa-user"></i>
					<span itemprop="name" data-toggle="tooltip" title="Written by ">IMSI Design</span>	</dd>			
			
							<dd class="category-name">
	<i class="fa fa-folder-open-o"></i>
				<span itemprop="genre" itemprop="genre" data-toggle="tooltip" title="Article Category">Blog</span>	</dd>			
			
							<dd class="published">
	<i class="fa fa-calendar-o"></i>
	<time datetime="2017-05-24T21:16:33+00:00" itemprop="datePublished" data-toggle="tooltip" title="Published Date">
		24 May 2017	</time>
</dd>			
		
					
			
					
		
	</dl>
  
        	</div>

	    
	
				
			<div itemprop="articleBody">
		<p>With the release of TurboCAD 2017, users will find a more seamless workflow between office design and editing, and field work documentation. Office staff can now take TurboCAD drawing sand upload them to an iOS mobile device, where the files can then be used with IMSI Design’s field documentation app, TurboSite, in order to capture project and site information. Perfect for building construction, site design, job site inspections, construction administration, as-built verification, or practically any other AEC-related fieldwork project.</p>
<p>The captured information can then be round-tripped back into TurboCAD 2017 through the program’s new ability to read TurboSite’s native TAP files. Export the TAP file back to the office via email or upload to DropBox, Box or any WebDAV-enabled cloud site.</p>
<p>Back in the office, you can open up the TAP file in the new GeoMarks Palette. The Tap file will contain all mark-ups and GeoMarks, which include all captured media such as photos, videos and notes taken onsite.</p>
<p>Noted changes from the field visit that need to be made to the original file can then easily be made in TurboCAD.</p>
<p>To see a real-world example of workflow integration with TurboCAD and TurboSite, watch this video:</p>
<p><iframe src="https://www.youtube.com/embed/QrCQpTR2wmY" width="800" height="450" frameborder="0" allowfullscreen=""></iframe></p>
<p>Working with TurboCAD and TurboSite together allows you to drive project efficiency across more of the project lifecycle, streamlining workflow processes and increasing productivity.</p>	</div>

	    	<div style="margin:12px auto;" class="clearfix"></div>
                	<div class="tags">
	    <span><i class="fa fa-tags hasTooltip" title="Tags"></i></span>
																	<a href="/us/tags/turbosite" class="" rel="tag">TurboSite</a>
				,	
																								<a href="/us/tags/turbocad" class="" rel="tag">TurboCAD</a>
				,	
																								<a href="/us/tags/cad-workflow" class="" rel="tag">CAD Workflow</a>
					
										</div>
        
    	<div class="helix-social-share">
		<div class="helix-social-share-blog helix-social-share-article">
			<ul>
								<li>
					<div class="facebook" data-toggle="tooltip" data-placement="top" title="Share On Facebook">

						<a class="facebook" onClick="window.open('http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/cad-workflow-integration-between-the-office-and-the-field','Facebook','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/cad-workflow-integration-between-the-office-and-the-field">
							<i class="fa fa-facebook-square"></i> Facebook						</a>

					</div>
				</li>
                								<li>
					<div class="twitter" data-toggle="tooltip" data-placement="top" title="Share On Twitter">
						<a class="twitter" onClick="window.open('http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/cad-workflow-integration-between-the-office-and-the-field&amp;text=CAD Workflow Integration Between the Office and the Field','Twitter share','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/cad-workflow-integration-between-the-office-and-the-field&amp;text=CAD%20Workflow%20Integration%20Between%20the%20Office%20and%20the%20Field">
							<i class="fa fa-twitter-square"></i> Twitter						</a>
					</div>
				</li>
                                				<li>
					<div class="google-plus">
						<a class="google-plus" data-toggle="tooltip" data-placement="top" title="Share On Google Plus" onClick="window.open('https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/cad-workflow-integration-between-the-office-and-the-field','Google plus','width=585,height=666,left='+(screen.availWidth/2-292)+',top='+(screen.availHeight/2-333)+''); return false;" href="https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/cad-workflow-integration-between-the-office-and-the-field" >
						<i class="fa fa-google-plus"></i></a>
					</div>
				</li>
				                				<li>
					<div class="linkedin">
						<a class="linkedin" data-toggle="tooltip" data-placement="top" title="Share On Linkedin" onClick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/cad-workflow-integration-between-the-office-and-the-field','Linkedin','width=585,height=666,left='+(screen.availWidth/2-292)+',top='+(screen.availHeight/2-333)+''); return false;" href="http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/cad-workflow-integration-between-the-office-and-the-field" >	
						<i class="fa fa-linkedin-square"></i></a>
					</div>
				</li>
                			</ul>
		</div>		
	</div> <!-- /.helix-social-share -->
    	<div style="margin:0 auto 35px;" class="clearfix"></div><hr />
        
        			
				    
    
	</article>";s:4:"head";a:11:{s:5:"title";s:25:"IMSI Design - IMSI Design";s:11:"description";s:227:"With the release of TurboCAD 2017, users will find a more seamless workflow between office design and editing, and field work documentation. Office staff can now take TurboCAD drawing sand upload them to an iOS mobile device...";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:4:"name";a:3:{s:8:"keywords";s:16:"CAD, Integration";s:6:"rights";N;s:6:"author";s:11:"IMSI Design";}}s:5:"links";a:0:{}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:0:{}s:6:"script";a:0:{}s:6:"custom";a:7:{i:0;s:135:"<meta property="og:url" content="http://test.imsitechnologies.com/us/blog/cad-workflow-integration-between-the-office-and-the-field" />";i:1;s:45:"<meta property="og:type" content="article" />";i:2;s:96:"<meta property="og:title" content="CAD Workflow Integration Between the Office and the Field" />";i:3;s:203:"<meta property="og:description" content="With the release of TurboCAD 2017, users will find a more seamless workflow between office design and editing, and field work documentation. Office staff c..." />";i:4;s:119:"<meta property="og:image" content="http://test.imsitechnologies.com/images/blog/cad-workflow-turbocad-turbosite.png" />";i:5;s:48:"<meta property="og:image:width" content="600" />";i:6;s:49:"<meta property="og:image:height" content="315" />";}s:10:"scriptText";a:0:{}}s:13:"mime_encoding";s:9:"text/html";s:7:"pathway";a:0:{}s:6:"module";a:0:{}}