<?php die("Access Denied"); ?>#x#a:5:{s:4:"body";s:15321:"<article class="item item-page" itemscope itemtype="http://schema.org/Article">
	<meta itemprop="inLanguage" content="en-US" />
		<div class="entry-gallery"><ul id="post-slider-996" class="list-unstyled post-slides"><li class="item active"><img src="images/2017/05/18/conceptual-design-product-visualization-turbocad-case-study.png" alt="IMSI Design"></li><li class="item"><img src="images/2017/05/18/revvll-one-concept-design-turbocad-kras-design.png" alt="IMSI Design"></li><li class="item"><img src="images/2017/05/18/conceptual-design-real-life-design-turbocad-kras-design.png" alt="IMSI Design"></li><li class="item"><img src="images/2017/05/18/concept-design-nexus-fitness-equipment-vs-reality-kras-design-turbocad.png" alt="IMSI Design"></li></ul></div><script type="text/javascript">
 jQuery(function($){
	var $post_slider = $("#post-slider-996");
	var $post_slide_height = $post_slider.height();
	$post_slider.imagesLoaded(function(){
	$post_slider.css({'height': $post_slide_height + 'px'}).addClass("img_loaded");
	$($post_slider).lightSlider({
		prevHtml:'<i style="width:44px;height:44px;margin-top:-22px;font-size:44px;background:rgba(10,10,10,.3);" class="pe pe-7s-angle-left"></i>',
		nextHtml:'<i style="width:44px;height:44px;margin-top:-22px;font-size:44px;background:rgba(10,10,10,.3);" class="pe pe-7s-angle-right"></i>',
		item:1,
		slideMove:1,
		loop:true,
		slideMargin:0,		
		pauseOnHover:true,
		auto:true,
		pause:5000,
		speed:700,
		pager:false,		
		cssEasing: 'cubic-bezier(0.75, 0, 0.35, 1)',
		adaptiveHeight:true,
		keyPress:true,
		responsive:[{breakpoint:768,settings:{item:1}},{breakpoint:480,settings:{item:1}}]
		});
	});
});
</script>        
	<div class="entry-header has-post-format">
		                            					            <span class="post-format"><i class="fa fa-picture-o"></i></span>
                                        <h2 itemprop="name">
                                            The Use of TurboCAD in Conceptual Design &amp; Product Visualization                                    </h2>
             
            
            	<dl class="article-info">

		
			<dt class="article-info-term"></dt>	
				
							<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
	<i class="fa fa-user"></i>
					<span itemprop="name" data-toggle="tooltip" title="Written by ">IMSI Design</span>	</dd>			
			
							<dd class="category-name">
	<i class="fa fa-folder-open-o"></i>
				<span itemprop="genre" itemprop="genre" data-toggle="tooltip" title="Article Category">Case Studies</span>	</dd>			
			
							<dd class="published">
	<i class="fa fa-calendar-o"></i>
	<time datetime="2017-05-18T16:33:09+00:00" itemprop="datePublished" data-toggle="tooltip" title="Published Date">
		18 May 2017	</time>
</dd>			
		
					
			
					
		
	</dl>
  
        	</div>

	    
	
				
			<div itemprop="articleBody">
		
<blockquote>Thanks to TurboCAD Pro Platinum, we have satisfied customers, and we can present our designs and promotional work much better – Ralf Kras, Owner</blockquote>
<h4>Background:</h4>
<p>Netherlands-based Kras.Design is a full-service design studio and workshop owned and operated by Ralf and Simone Kras. The company specializes in designing aesthetic and mechanical solutions, primarily geared towards the fitness industry and the show- and medical trailer industry. By working with a group of freelancers, photographers, and engineers, Kras.Design is able to meet basically any customer need or requirement, from the first pen stroke, to prototype creation and beyond.</p>
<h4>Challenges &amp; Product Search:</h4>
<p>After several years of using an old version of AutoCAD, a new CNC machine required Kras.Design to upgrade to a 3D design solution with CAM (Computer-Aided Machining) abilities. With the old 2D CAD software, the company’s work processes were also both quite inefficient and time-consuming. "It became clear that we needed a good 3D technical drawing program for the development and designs of our products," Ralf Kras explains.</p>
<blockquote>
<p>Price-wise, TurboCAD is much more attractive than other engineering 3D software. It can do all the things that AutoCAD and Solidworks can do, but at a far more friendly price.</p>
</blockquote>
<p>When Kras.Design started looking for new CAD software, their criteria initially included 3D design capabilities and CAD/CAM functionality. Later on, the company also realized the benefits of animation when it comes to concept visualization and presentation. Ralf and Simone Kras did their CAD software research online. "First we were looking at AutoCAD and Solidworks,” Ralf Kras explains, "but the prices for these programs were very high, especially with CAD/CAM capabilities incorporated." Ultimately, the product search led Kras.Design to TurboCAD, after Ralf Kras recalled using it in the 1990s. "When I was working for my father’s company, we used TurboCAD v3," Kras says, and explains that he looked TurboCAD up on the Internet and realized that it had evolved into a program that seemed able to meet all their requirements. In addition to having the 3D design features that the company needed, TurboCAD also had a more attractive price-point than many of its competitors. "Price-wise, TurboCAD is much more attractive than other engineering 3D software," Kras says. "It can do all the things that AutoCAD and Solidworks can do, but at a far more friendly price."</p>
<h4>Implementation &amp; Product Use:</h4>
<p>Despite having some basic knowledge of a very early version of TurboCAD, there were some initial challenges when Kras.Design first started using the software. "There are always some challenges with new design software, but with the tutorials and manuals, TurboCAD is both fun and rewarding to learn," Ralf Kras says. "I am still learning and discovering new tools, so the fun never stops!"</p>
<p>At this point, Kras.Design uses TurboCAD Pro Platinum for a wide variety of 3D modeling and design projects, primarily using the software’s mechanical toolset. The company works almost exclusively in 3D and in model space. Additionally, the company uses TurboCAD plugins AnimationLab and TurboCAD CAM to further extend the use of the software.</p>
<p>For show and medical trailer design, the Kras.Designs team designs possible concepts that meet the needs of its clients. "We use AnimationLab to make a presentation in order to help our clients to visualize the design and its technical abilities," Ralf Kras explains. "When a concept trailer is sold, we work with the two largest trailer builders in Europe to design the new technical solutions to make the trailers work the way they do in the presentation." The trailer manufacturers use Solidworks and AutoCAD; however, with the extensive import and export capabilities of TurboCAD, this is not a problem, Ralf Kras says.</p>
<p>As far as the fitness market goes, one of Kras.Design’s long-term projects is the Revvll. The Revvll is a fitness device that is now sold all over the world in large quantities, Ralf Kras says, and encourages readers to search for it on YouTube to see hundreds of videos showing the product in action. "For the Revvll project, we were contacted three years ago by a German company, Aerobis gmbh, too see if we could design and prototype a portable rope-climbing machine," he explains. "We made a first concept design with TurboCAD, and the presentation in AnimationLab."</p>
<p>Both the concept design and the animated presentation made it clear to Aerobis that Kras.Design was able to create a very aesthetically pleasing product weighing no more than 5 kilograms, and creating more resistance than the competition’s stationary climbing machines. "They were immediately convinced that we were the right partner to do this project," Ralf Kras says.</p>
<blockquote>
<p>To implement such technology into a small device that weighs 5 kilograms is a mechanical challenge, but we succeeded with the help of TurboCAD.</p>
</blockquote>
<p>The next step was to design the first Revvll prototype, using Eddy current as a braking system to create the resistance needed for the exercise. This was a challenging task. "To implement such technology into a small device that weighs 5 kilograms is a mechanical challenge," Ralf Kras says, "but we succeeded with the help of TurboCAD."</p>
<p>Kras.Design produced all the parts on its CNC machine, controlled by TurboCAD CAM, and then assembled and tested the first unit. "After a bit of tweaking and several months of testing, we not only successfully delivered the first pre-production Revvll, but we also got the order to produce the first 300 units," Ralf Kras remembers.</p>
<p>At this point, three years and several iterations of the Revvll later, the product is such a success that Kras.Design is no longer able to produce the devices in the required batch numbers. Instead, the company has outsourced the production. "We are still developing the Revvll and other products for Aerobis, which is now one of our biggest clients," Kras says.</p>
<h4>Results &amp; Benefits:</h4>
<p>From a general business perspective, the implementation and use of TurboCAD has helped bring Kras.Design an increased number of orders for both trailer concept designs and orders from the fitness branch. "Strength equipment companies want to re-design their fitness lines with our new NEXUS technology, which was designed with TurboCAD," Ralf Kras says. By significantly improving efficiency, TurboCAD also helps Kras.Design meet the increased demand that the company is experiencing. "We can get presentations and visualizations out within a week after the first talks with potential clients," Ralf Kras says, "This is crucial when in competition with other designers."</p>
<blockquote>
<p>We can get presentations and visualizations out within a week after the first talks with potential clients. This is crucial when in competition with other designers.</p>
</blockquote>
<p>Other benefits of implementing TurboCAD into its work processes include the ability to avoid design and prototype conflicts. "With the 3D software from TurboCAD, we avoid conflicts between the design- and prototype phase thanks to the software’s CNC capabilities," Ralf Kras explains. However, what Kras.Design considers the most important benefit of the software is TurboCAD’s visualization tools and capabilities. "We are able to communicate designs, visualizations, and presentations in such a way that it often leaves our clients baffled," Ralf Kras says. "They are always able to understand and appreciate our designs." The TurboCAD AnimationLab plug-in plays a big part in product visualization for Kras.Design, and the introduction of this product further enhanced the company’s ability to successfully present a product to a client.</p>
<p>Looking towards the future, Kras.Design sees a need to find additional TurboCAD talents in order to meet the expected workflow. There are also plans for the company to develop its own products. "I have a lot of products on the shelf that I want to develop in the near future, and TurboCAD will have a part in all of them” Ralf Kras says, and finishes with a smile: "Perhaps if you develop the TurboTimeMachine add-on, that would help."</p>
<p>To others considering the software, Ralf Kras recommends comparing the features and capabilities of TurboCAD to those of other CAD programs, and then reaching a conclusion based on the findings. "The endless design possibilities of TurboCAD are not in any way less than software five or six times as expensive,"he says. "You would basically be a thief of your own money to buy other software!"</p>	</div>

	    
    	<div class="helix-social-share">
		<div class="helix-social-share-blog helix-social-share-article">
			<ul>
								<li>
					<div class="facebook" data-toggle="tooltip" data-placement="top" title="Share On Facebook">

						<a class="facebook" onClick="window.open('http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/case-studies/the-use-of-turbocad-in-conceptual-design-product-visualization','Facebook','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/case-studies/the-use-of-turbocad-in-conceptual-design-product-visualization">
							<i class="fa fa-facebook-square"></i> Facebook						</a>

					</div>
				</li>
                								<li>
					<div class="twitter" data-toggle="tooltip" data-placement="top" title="Share On Twitter">
						<a class="twitter" onClick="window.open('http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/the-use-of-turbocad-in-conceptual-design-product-visualization&amp;text=The Use of TurboCAD in Conceptual Design & Product Visualization','Twitter share','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/the-use-of-turbocad-in-conceptual-design-product-visualization&amp;text=The%20Use%20of%20TurboCAD%20in%20Conceptual%20Design%20&%20Product%20Visualization">
							<i class="fa fa-twitter-square"></i> Twitter						</a>
					</div>
				</li>
                                				<li>
					<div class="google-plus">
						<a class="google-plus" data-toggle="tooltip" data-placement="top" title="Share On Google Plus" onClick="window.open('https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/the-use-of-turbocad-in-conceptual-design-product-visualization','Google plus','width=585,height=666,left='+(screen.availWidth/2-292)+',top='+(screen.availHeight/2-333)+''); return false;" href="https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/case-studies/the-use-of-turbocad-in-conceptual-design-product-visualization" >
						<i class="fa fa-google-plus"></i></a>
					</div>
				</li>
				                				<li>
					<div class="linkedin">
						<a class="linkedin" data-toggle="tooltip" data-placement="top" title="Share On Linkedin" onClick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/case-studies/the-use-of-turbocad-in-conceptual-design-product-visualization','Linkedin','width=585,height=666,left='+(screen.availWidth/2-292)+',top='+(screen.availHeight/2-333)+''); return false;" href="http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/case-studies/the-use-of-turbocad-in-conceptual-design-product-visualization" >	
						<i class="fa fa-linkedin-square"></i></a>
					</div>
				</li>
                			</ul>
		</div>		
	</div> <!-- /.helix-social-share -->
    	<div style="margin:0 auto 35px;" class="clearfix"></div><hr />
        
        			
	
<nav role="pagination">
    <ul class="cd-pagination no-space animated-buttons custom-icons">
		            <li class="button btn-previous">
                <a href="/us/blog/case-studies/turbocad-helps-electronic-engineering-consultant-expand-into-mechanical-cad" rel="prev"><i>Prev</i></a>
            </li>
                
           </ul>
</nav>
				    
    
	</article>";s:4:"head";a:11:{s:5:"title";s:25:"IMSI Design - IMSI Design";s:11:"description";s:219:"IMSI Design is a leader in affordable, general-purpose CAD (Computer Aided Design) and home design desktop software, and a pioneer in mobile solutions for the AEC (Architectural, Engineering, and Construction) industry.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:4:"name";a:3:{s:8:"keywords";s:39:"IMSI Design, CAD, TurboCAD, home design";s:6:"rights";N;s:6:"author";s:11:"IMSI Design";}}s:5:"links";a:0:{}s:11:"styleSheets";a:0:{}s:5:"style";a:1:{s:8:"text/css";s:76:".lSPrev,.lSNext{height:44px;width:44px;}.lSPrev>i,.lSNext>i{font-size:44px;}";}s:7:"scripts";a:4:{s:27:"/media/jui/js/jquery.min.js";a:2:{s:4:"type";s:15:"text/javascript";s:7:"options";a:6:{s:7:"version";s:4:"auto";s:8:"relative";b:1;s:11:"detectDebug";b:0;s:9:"framework";b:0;s:8:"pathOnly";b:0;s:13:"detectBrowser";b:1;}}s:34:"/media/jui/js/jquery-noconflict.js";a:2:{s:4:"type";s:15:"text/javascript";s:7:"options";a:6:{s:7:"version";s:4:"auto";s:8:"relative";b:1;s:9:"framework";b:0;s:8:"pathOnly";b:0;s:13:"detectBrowser";b:1;s:11:"detectDebug";b:1;}}s:35:"/media/jui/js/jquery-migrate.min.js";a:2:{s:4:"type";s:15:"text/javascript";s:7:"options";a:6:{s:7:"version";s:4:"auto";s:8:"relative";b:1;s:11:"detectDebug";b:0;s:9:"framework";b:0;s:8:"pathOnly";b:0;s:13:"detectBrowser";b:1;}}s:38:"/templates/flex/js/imagesloaded.min.js";a:2:{s:4:"type";s:15:"text/javascript";s:7:"options";a:0:{}}}s:6:"script";a:0:{}s:6:"custom";a:7:{i:0;s:153:"<meta property="og:url" content="http://test.imsitechnologies.com/us/blog/case-studies/the-use-of-turbocad-in-conceptual-design-product-visualization" />";i:1;s:45:"<meta property="og:type" content="article" />";i:2;s:103:"<meta property="og:title" content="The Use of TurboCAD in Conceptual Design & Product Visualization" />";i:3;s:203:"<meta property="og:description" content="Netherlands-based Kras.Design specializes in designing aesthetic and mechanical solutions, and uses TurboCAD for conceptual design and product visualizatio..." />";i:4;s:155:"<meta property="og:image" content="http://test.imsitechnologies.com/images/Case-Studies/conceptual-design-product-visualization-turbocad-case-study.png" />";i:5;s:48:"<meta property="og:image:width" content="600" />";i:6;s:49:"<meta property="og:image:height" content="315" />";}s:10:"scriptText";a:0:{}}s:13:"mime_encoding";s:9:"text/html";s:7:"pathway";a:0:{}s:6:"module";a:0:{}}