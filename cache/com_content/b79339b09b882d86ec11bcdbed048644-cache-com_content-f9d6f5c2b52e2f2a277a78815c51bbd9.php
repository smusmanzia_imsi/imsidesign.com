<?php die("Access Denied"); ?>#x#a:5:{s:4:"body";s:10044:"<article class="item item-page" itemscope itemtype="http://schema.org/Article">
	<meta itemprop="inLanguage" content="en-US" />
		
		<div class="entry-image full-image"> <img
				src="images/blog/turbocad-deluxe-2d-3d-2017-over-20-new-improved-features.png" alt="" itemprop="image"/> </div>
        
	<div class="entry-header has-post-format">
		                            					            <span class="post-format"><i style="margin-right:-6px;" class="fa fa-pencil-square-o"></i></span>
                                        <h2 itemprop="name">
                                            IMSI Design Releases TurboCAD Deluxe 2017                                    </h2>
             
            
            	<dl class="article-info">

		
			<dt class="article-info-term"></dt>	
				
							<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
	<i class="fa fa-user"></i>
					<span itemprop="name" data-toggle="tooltip" title="Written by ">IMSI Design</span>	</dd>			
			
							<dd class="category-name">
	<i class="fa fa-folder-open-o"></i>
				<span itemprop="genre" itemprop="genre" data-toggle="tooltip" title="Article Category">Press Releases</span>	</dd>			
			
							<dd class="published">
	<i class="fa fa-calendar-o"></i>
	<time datetime="2017-05-05T17:03:00+00:00" itemprop="datePublished" data-toggle="tooltip" title="Published Date">
		05 May 2017	</time>
</dd>			
		
					
			
					
		
	</dl>
  
        	</div>

	    
	
				
			<div itemprop="articleBody">
		
<p>NOVATO, Calif., May 5, 2017 – IMSI® Design, a leading CAD software developer, announced today the release of TurboCAD Deluxe 2017, a complete 2D drafting and 3D modeling application for Windows® desktop PCs.</p>
<p>"TurboCAD Deluxe 2017 continues to be the best solution on the market for new 2D/3D CAD users. It’s easy to learn and use and offers an incredible collection of 2D drafting, 3D surface modeling, photorealistic rendering, and supported file filters," reports Bob Mayer, President of IMSI Design.</p>
<p>Over 20 new features and major improvements have been added to TurboCAD Deluxe 2017 throughout the program including:</p>
<h4>Usability &amp; Interface</h4>
<ul>
<li>New Image Management Palette – Adds simple drag-and-drop insertion, control of image parameters, and embedding.</li>
<li>New Table Editing / Attachable Data – Build tables by making (non-OLE) connections to .CSV and Excel files. Simply edit table layout, format, and content, now similar to Microsoft Word.</li>
<li>New Timestamp – For security and collaboration, file metadata now includes Date of Creation, Date of Last Modification, Total File Editing Time, and Total Time of Last Session</li>
</ul>
<h4>2D Drafting &amp; Editing Features</h4>
<ul>
<li>New Center Line and Center Mark – Create associative center lines and center marks.</li>
<li>Improved Multi-Text Editor – New text editor interface with new formatting features and new options including numbered lists, bulleted lists, and multi-column support.</li>
<li>New Scale by Two Points – Scale a set of objects to a specific size by using two reference points.</li>
<li>Improved Intelligent Scaling – Create hatches, text, and dimensions that automatically scale to the current zoom level of the view.</li>
</ul>
<h4>Architectural Design Tools</h4>
<ul>
<li>New Copying of Architectural Objects – Use the copy and array tools to insert a set of windows or doors into a wall for greater productivity.</li>
<li>Improved House Builder Wizard – Create rooms on multiple workplanes for quick design of multi-story houses.</li>
</ul>
<h4>Photorealistic Rendering &amp; Visualization</h4>
<ul>
<li>New Redsdk 4.2 Engine Migration – Improved photorealistic rendering with new shaders, more realistic materials, new expanded parameters to control anti-ailiasing, caustics for sun lighting, physical sky lighting, geo-located sun positioning, and volumetric effects.</li>
</ul>
<h4>File Support / Interoperability</h4>
<ul>
<li>IImproved AutoCAD (DXF, DWG, and DWF) File Compatibility – Implementation of the latest Teigha engine.</li>
<li>Improved SketchUp (SKP) File Support – Import geometry, views, materials, and components from SketchUp files created in SketchUp Pro or SketchUp Mark versions 3.0 to 2017.</li>
<li>New Google Earth (KML and KMZ) File Support – Import KML and KMZ files into TurboCAD to view your designs in Google Earth independent of the web.</li>
<li>New Mobile App Support - Import and View TurboApp (TAP) files from mobile devices.</li>
</ul>
<h3>Availablitily &amp; Pricing</h3>
<p>TurboCAD Deluxe 2017 is available for $149.99 USD for a full, permanent license.</p>
<p>For more information, download a 30-day trial, or to upgrade online, please visit <a href="http://www.TurboCAD.com">www.TurboCAD.com</a> or call IMSI Design at 1.800.833.8082 (+1.415.483.8000).</p>
<h3>About IMSI Design</h3>
<p>IMSI Design is a leader in affordable, general-purpose CAD (Computer Aided Design), and a pioneer in mobile solutions for the AEC (Architectural, Engineering, and Construction) industry. With over 16 million products distributed since 1988, IMSI Design products include the TurboCAD®, DesignCAD™, TurboFloorPlan®, TurboViewer®, TurboReview®, and TurboSite® families of precision design applications for desktop and mobile. Please visit <a href="http://www.IMSIdesign.com">www.IMSIdesign.com</a> for more information.</p>
<p><strong>Contact:</strong></p>
<p>IMSI Design<br /> Pam Volpe<br /> 415.483.8025<br /> <span id="cloak3d7ed54e9ea2dd4756919e7eb4eb76f7">JLIB_HTML_CLOAKING</span><script type='text/javascript'>
				document.getElementById('cloak3d7ed54e9ea2dd4756919e7eb4eb76f7').innerHTML = '';
				var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
				var path = 'hr' + 'ef' + '=';
				var addy3d7ed54e9ea2dd4756919e7eb4eb76f7 = 'pr' + '&#64;';
				addy3d7ed54e9ea2dd4756919e7eb4eb76f7 = addy3d7ed54e9ea2dd4756919e7eb4eb76f7 + '&#105;ms&#105;d&#101;s&#105;gn' + '&#46;' + 'c&#111;m';
				var addy_text3d7ed54e9ea2dd4756919e7eb4eb76f7 = 'pr' + '&#64;' + '&#105;ms&#105;d&#101;s&#105;gn' + '&#46;' + 'c&#111;m';document.getElementById('cloak3d7ed54e9ea2dd4756919e7eb4eb76f7').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy3d7ed54e9ea2dd4756919e7eb4eb76f7 + '\'>'+addy_text3d7ed54e9ea2dd4756919e7eb4eb76f7+'<\/a>';
		</script></p>
<p>###</p>
<p>© 2017 IMSI Design, LLC. All rights reserved. IMSI and TurboCAD are registered trademarks and/or trademarks of IMSI Design, LLC. AutoCAD is a registered trademark of Autodesk. All other identifiable marks are the property of their respective owners.</p>	</div>

	    
    	<div class="helix-social-share">
		<div class="helix-social-share-blog helix-social-share-article">
			<ul>
								<li>
					<div class="facebook" data-toggle="tooltip" data-placement="top" title="Share On Facebook">

						<a class="facebook" onClick="window.open('http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-releases-turbocad-deluxe-2017','Facebook','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://www.facebook.com/sharer.php?u=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-releases-turbocad-deluxe-2017">
							<i class="fa fa-facebook-square"></i> Facebook						</a>

					</div>
				</li>
                								<li>
					<div class="twitter" data-toggle="tooltip" data-placement="top" title="Share On Twitter">
						<a class="twitter" onClick="window.open('http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-releases-turbocad-deluxe-2017&amp;text=IMSI Design Releases TurboCAD Deluxe 2017','Twitter share','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://twitter.com/share?url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-releases-turbocad-deluxe-2017&amp;text=IMSI%20Design%20Releases%20TurboCAD%20Deluxe%202017">
							<i class="fa fa-twitter-square"></i> Twitter						</a>
					</div>
				</li>
                                				<li>
					<div class="google-plus">
						<a class="google-plus" data-toggle="tooltip" data-placement="top" title="Share On Google Plus" onClick="window.open('https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-releases-turbocad-deluxe-2017','Google plus','width=585,height=666,left='+(screen.availWidth/2-292)+',top='+(screen.availHeight/2-333)+''); return false;" href="https://plus.google.com/share?url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-releases-turbocad-deluxe-2017" >
						<i class="fa fa-google-plus"></i></a>
					</div>
				</li>
				                				<li>
					<div class="linkedin">
						<a class="linkedin" data-toggle="tooltip" data-placement="top" title="Share On Linkedin" onClick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-releases-turbocad-deluxe-2017','Linkedin','width=585,height=666,left='+(screen.availWidth/2-292)+',top='+(screen.availHeight/2-333)+''); return false;" href="http://www.linkedin.com/shareArticle?mini=true&url=http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-releases-turbocad-deluxe-2017" >	
						<i class="fa fa-linkedin-square"></i></a>
					</div>
				</li>
                			</ul>
		</div>		
	</div> <!-- /.helix-social-share -->
    	<div style="margin:0 auto 35px;" class="clearfix"></div><hr />
        
        			
	
<nav role="pagination">
    <ul class="cd-pagination no-space animated-buttons custom-icons">
		        
                   <li class="button btn-next">
                <a href="/us/blog/press-releases/imsi-design-introduces-turbocad-expert-2017" rel="next"><i>Next</i></a>
            </li>
            </ul>
</nav>
				    
    
	</article>";s:4:"head";a:11:{s:5:"title";s:25:"IMSI Design - IMSI Design";s:11:"description";s:219:"IMSI Design is a leader in affordable, general-purpose CAD (Computer Aided Design) and home design desktop software, and a pioneer in mobile solutions for the AEC (Architectural, Engineering, and Construction) industry.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:4:"name";a:3:{s:8:"keywords";s:39:"IMSI Design, CAD, TurboCAD, home design";s:6:"rights";N;s:6:"author";s:11:"IMSI Design";}}s:5:"links";a:0:{}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:0:{}s:6:"script";a:0:{}s:6:"custom";a:7:{i:0;s:134:"<meta property="og:url" content="http://test.imsitechnologies.com/us/blog/press-releases/imsi-design-releases-turbocad-deluxe-2017" />";i:1;s:45:"<meta property="og:type" content="article" />";i:2;s:80:"<meta property="og:title" content="IMSI Design Releases TurboCAD Deluxe 2017" />";i:3;s:206:"<meta property="og:description" content="NOVATO, Calif., May 5, 2017 – IMSI® Design, a leading CAD software developer, announced today the release of TurboCAD Deluxe 2017, a complete 2D drafting a..." />";i:4;s:144:"<meta property="og:image" content="http://test.imsitechnologies.com/images/blog/turbocad-deluxe-2d-3d-2017-over-20-new-improved-features.png" />";i:5;s:48:"<meta property="og:image:width" content="600" />";i:6;s:49:"<meta property="og:image:height" content="315" />";}s:10:"scriptText";a:0:{}}s:13:"mime_encoding";s:9:"text/html";s:7:"pathway";a:0:{}s:6:"module";a:0:{}}